SHELL = bash
.PHONY: install session
session:
	./krp.py --setup-expansion > session
	@echo 'Please type: source session'
install:
	@if [ $$UID -gt 0 ]; then echo 'Must be run as root.'; exit 1; fi
	./krp.py --setup-expansion > /etc/bash_completion.d/kerbal-rapid-prototyping.sh

parts.py:extract_parts.sh
	@if [ -f '$@' ]; then echo 'Please manually delete parts.py to proceed.'; exit 1; else ./extract_parts.sh > parts.py; fi
