# KRP - Kerbal Rapid Prototyping

Try out how a rocket will behave before building it. It comes with a micro-language to specify a staged rocket.

## Table of Contents

<!--ts-->
   * [KRP - Kerbal Rapid Prototyping](#krp---kerbal-rapid-prototyping)
      * [Table of Contents](#table-of-contents)
      * [Getting Started](#getting-started)
         * [Prerequisites](#prerequisites)
         * [First Run](#first-run)
         * [Auto Completion](#auto-completion)
      * [Specifying Rockets](#specifying-rockets)
      * [More Advanced Staging](#more-advanced-staging)
         * [Phantom Engines](#phantom-engines)
         * [Example](#example)
         * [Mass Qualifiers](#mass-qualifiers)
      * [Grouping](#grouping)
         * [Sub-Groups](#sub-groups)
         * [Piggy Back Groups](#piggy-back-groups)
         * [Sub-Assemblies](#sub-assemblies)
      * [Plots](#plots)
         * [Example: Deep-Space probe](#example-deep-space-probe)
         * [Example: Launching](#example-launching)
         * [Example: Very Weird Contraption](#example-very-weird-contraption)
         * [A Study of different Engine technologies](#a-study-of-different-engine-technologies)
      * [Documentation / Language Specification](#documentation--language-specification)
      * [Experimental](#experimental)
         * [Auto-build rocket](#auto-build-rocket)
         * [Read .craft file](#read-craft-file)
      * [Contributing](#contributing)
      * [Authors](#authors)
      * [License](#license)
      * [Known Issues](#known-issues)
      * [Jool Rocket Example Output](#jool-rocket-example-output)



<!--te-->

## Getting Started

Clone the [repository](https://gitlab.com/bitzoid/krp)

    git clone https://gitlab.com/bitzoid/krp.git

or manually download the [zip archive](https://gitlab.com/bitzoid/krp/-/archive/master/krp-master.zip) on to your local machine.

### Prerequisites

Requires Python3. It is also *highly* recommended to install the termcolor python3 package. If python3-matplotlib is installed you can plot graphs.

You can install these under debian, mint and ubuntu with:

    sudo apt-get install python3 python3-termcolor python3-matplotlib


### First Run

For a simple test of a tiny one-staged ship, run:

    ./krp.py 1.2t + baguette + spark

![Output of tiny spark ship](.example-images/example_spark_output.png)


It colour-highlights the acceleration, dv, and isp and greys-out the location where the rocket is probably not fired (only for multi-stage ships).

If you need help or for an overview of commands, run

    ./krp.py --help

### Auto Completion

You can actually enable auto-completion for the tool by saying:

    make session
    source session

If you want to enable it permanently you can properly install it:

    sudo make install

This will keep the script in the current directory, but enable auto completion in bash globally.

## Specifying Rockets

Try:

    1t + t400 + reliant

for a one-stage rocket that consists of 1t payload, a FL-T400 liquid fuel+oxigen tank and a LV-T30 Reliant engine. You will learn that it is insufficient to reach orbit, but can lift off the launchpad. Let's add a lifting stage, then:

    1t + t400 + reliant, 200kg + 2*rocko64 + mainsail

This adds a stage consisting of two big-orange-tanks, a mainsail and some glue parts worth 0.2t (200kg), that lifts the tiny 4.5t ship well into orbit. Try out what happens if you add 20 tanks instead of 2 (spoiler: you wont get to the Mün).

So, stages are separated by a comma (`,`).

You can list all engines with the --engines switch and all tanks with --tanks.

## More Advanced Staging

Assume you have such a stage:

    50t + kerbo144 + rhino

but you want to add another stage with two further rhinos. However, you want the upper rhino to also fire in the lower stage, while consuming the lower stage fuel. You could say:

    50t + kerbo144 + rhino, 2*kerbo144 + 2*rhino + rhino

But the last one would add 9t that are not really there.

### Phantom Engines

For this there are phantom engines:

    50t + kerbo144 + rhino, 2*kerbo144 + 2*rhino + (rhino)

Now that last rhino fires in Stage 1 but does not add mass to the craft. Repeating yourself, however, is ugly. So you can just bulk-phantom-copy all engines from the previous stage with an exclamation mark:

    50t + kerbo144 + rhino, 2*kerbo144 + 2*rhino + !

However, note that if the previous stage has phantom engines, these will be copied as well (phantom-copy is recursive).

### Example

Here is a more involved example that builds a rocket that can travel to Jool and bring a crew of three kerbonauts back safely (with lf to spare).

    8.2t + rocko64_lf + 2*mk1 + 2*ncs + 2*nerv,
    2*mk3a + 2*kerbo144 + rhino,
    ! + 4*mk3a + 2*kerbo144 + 2*rhino,
    4*kerbo144 + 4*c7 + 4*rocko64 + 2*mammoth + 4*twinboar,
    ! + 4*c7 + 4*rocko64 + 4*twinboar

Check out the [output of this command](#jool-rocket-example-output).

### Mass Qualifiers

If you don't know the payload mass but only the total mass of a stage you can override the computed mass:

    10t + t800 + swivel : 17t, 1t + rocko32 + skipper : 42t

Normally, this two-stage rocket would only have a mass of 38t. So, if there are (e.g.) 4t that are unaccounted for, correct for it by qualifying a stage with a `:` followed by a mass spec.

*Please note:* The old chunk notation that overrides the mass of the entire rocket has been deprecated and removed in favour of the more powerful qualifiers discussed here.

## Grouping

### Sub-Groups

Sub-assemblies can be grouped with curly braces. These groups can have quantity multipliers like any other part.

    5t + mk3lf + 4*{ ncs + 2*mk1 + nerv }

Furthermore, sub-groups can have stages and sub-groups. Stages propagate out of the sub-group, as if the multiplier was applied to each stage individually.

### Piggy Back Groups

The rocket specification language also allows to specify piggy-back rockets. This is useful if you want to add a satellite to your rocket, but not have it fire during main staging. A sub-expression is similar to a sub-group but only its resulting total mass is added to the stage:

    1t + [ 400kg + xenon150 + dawn ] + 2*[ 1t + 5*{xenon150 + dawn} ] + rocko16 + poodle, 2*mk3a + kerbo72 + rhino, 3*kerbo144 + 2*twinboar + mammoth

Here, the three xenon probes are considered as raw mass, but not as a stage that is actually ignited. Sub-Expressions nest arbitrarily with sub-groups.

### Sub-Assemblies

You can save assemblies that you regularly use under a recognisable name. The following two commands are equivalent:

    ./krp.py --save stegosaurus '0.075t + 9*{ t100 + vector }'
    ./krp.py stegosaurus = '0.075t + 9*{ t100 + vector }'

This assembly will be stored under krp/assemblies/stegosaurus.krpa and can be used in any rocket expression as:

    400t + 3*{ kerebo512 + <stegosaurus> }

Note that angle brackets must be quoted as your shell will interpret them as pipes, otherwise.

## Plots

KRP can plot dv and acceleration for a series of parametrised rockets to make it easier to see how many one should add to reach a certain performance mark. By inserting the variable `\x` in place of multipliers, one can specify a variable number of some part, group or assembly. See `./krp.py --help plot` for more details.
You can also compare several configurations by separating them with semicola (`;`). See [A Study of different Engine technologies](#a-study-of-different-engine-technologies).

KRP automatically invokes `--plot [1,16]` if there is a variable present in an expression.

### Example: Deep-Space probe

How Xe tanks does a deep-space probe require to reach the dv requirements for a certain trip?

    ./krp.py --plot [1:6] '2t + 5*dawn + \x*xenon750'

![Plot of deep-space probe](.example-images/deep-space.jpg)

This tells us the maximal dry acceleration when dry (green) and the acceleration when full (red) as well as the expected dv (black).

### Example: Launching

If one mainsail does not cut it, add more vectors ...

    ./krp.py --plot [1:16] '100t + kerbo512 + mainsail + \x*{vector + 2*t800}' asl

![Plot of mainsail-vector lifter](.example-images/mainsail-vector-lifter.jpg)

As we can tell, the minimum number of engines to even move from the launchpad ASL, is achieved at `\x=4` (blue dotted lines). However, the peak dv is achieved at `\x=3` (grey dashed line).

### Example: Very Weird Contraption

The `\x` parameter can be used more than once, and also in sub-groups to specify non-linear configurations. Even tough the usefulness of the following craft is doubtful, we can learn that both dv and acceleration peak at a certain point and adding more actually reduces performance.

![Plot of very weird contraption](.example-images/weird.jpg)

### A Study of different Engine technologies

We can compare the performance of an array of configurations. Here we study how much dv we get with all things else being equal (mass, acceleration, thrust).

    ./krp.py --plot [0:1000] 'ion / blue = \x*500xe + 30*{dawn + 11*nuk}; ion no EC / turquoise = \x*500xe + 30*{dawn + 8800xe}; ion fc / teal = \x*{500*{0.4614xe + 0.0059ox + 0.0048lf}} + 30*dawn + 15*fca + 228000*{0.4614xe + 0.0059ox + 0.0048lf} ; nuclear / orange = \x*10lf + nerv + 6180lf; chemical / purple = \x*{4.5lf+5.5ox} + terrier + 6680*{0.45lf + 0.55ox}'

![Technology Study](.example-images/tech_study.png)

As we can see, when plotting several vessels, a label and colour can be specified for each with the syntax `label / colour = rocket_expr`.

## Documentation / Language Specification

For general help and all switches run

    ./krp.py --help

The syntax for specifying rockets, a list of engines and tanks and a list of bodies in the kerbol system is available through the following switches respectively:

* `--syntax` *Outputs a grammar for the rocket specification language.*
* `--engines` *Lists all known engines (stock + mhe) and their properties. Excluding monoprop and jet engines.*
* `--tanks` *Lists all known tanks (stock + mhe) and their properties. Excluding monoprop tanks.*
* `--planet` *Lists all stars, planets, dwarfs and moons in the kerbol system and their properties.*

## Experimental

### Auto-build rocket

The --build and --lko switches attempt to compute the necessary number of parts for a certain dv/acceleration. For instance:

    ./krp.py --lko 1 t800 swivel

Will estimate that you need 5 FL-T800 tanks and 2 LV-T45 Swivel engines to launch 1t into low Kerbin orbit. Especially in atmosphere this is just a very rough estimation. The more general --build can be more useful when in a vacuum.

### Read .craft file

krp can parse .craft files and build a rocket expression from them. However, currently parts may be assigned to the wrong stage or not at all.

    ./krp.py --craft my_awesome_rocket.craft

## Contributing

If you like the tool, share and extend it. If you send me a pull request, I'm happy to incorporate your improvements.

## Authors

* [bitzoid](https://gitlab.com/bitzoid)

## License

This project is licensed under the CC-BY-NC-SA 4.0 license - see the [LICENSE](LICENSE) file for details.

## Known Issues

* 'Approx. vertical distance' is a very rough estimation of how high you will get and gets more imprecise a) the more drag you have b) the higher you go. It also only computes how far you get, while the engine is still burning. You will of course continue to climb even if you throttle down the engine.
* As of now, no monoprop is implemented, i.e. thrusters and the puff engine are unsupported.
* As of now, no air-breathing engines are implemented (the nerv is, though).
* Shell expansion within sub-groups doesn't work. This is a bash issue.

## Jool Rocket Example Output

![Output of multi-staged Jool ship](.example-images/example_jool_output.png)
