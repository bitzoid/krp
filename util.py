# this was was inspired from
# https://matplotlib.org/gallery/images_contours_and_fields/image_annotated_heatmap.html
# numpy nonsense thrown out
def heatmap(plt, data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
  if not ax:
    ax = plt.gca()

  ax.set_xlabel('x')
  ax.set_ylabel('y')

  data = list(reversed(data))
  row_labels = list(reversed(row_labels))

  # Plot the heatmap
  im = ax.imshow(data, **kwargs)

  # Create colorbar
  cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
  cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

  ax.set_xticks(list(range(0,len(col_labels))))
  ax.set_yticks(list(range(0,len(row_labels))))
  ticks = [col_labels,row_labels]
  for i in range(0,len(ticks)):
    if len(ticks[i]) > 32:
      last = ticks[i][-1]
      ti = ticks[i][0::int(len(ticks[i])/20)]
      if ti[-1] != last:
        ti += [last]
      use = []
      for t in ticks[i]:
        if t in ti:
          use.append(str(t))
        else:
          use.append('')
      ticks[i] = use
  ax.set_xticklabels(ticks[0])
  ax.set_yticklabels(ticks[1])

  ax.tick_params(top=False, bottom=True,
                 labeltop=False, labelbottom=True)

  return im, cbar


def heatmap_annotate(plt, im, data=None, valfmt="{x:.2f}",
                     textcolors=["white", "black"],
                     threshold=None,
                     step=1):
  if type(step) == int:
    step = (step,step)

  data = list(reversed(data))

  from matplotlib import ticker

  if threshold == None:
    threshold = max(map(max,data))-min(map(min,data))/2.

  if type(valfmt) == str:
    valfmt = ticker.StrMethodFormatter(valfmt)

  coords = [(i,j) for i in range(0,len(data),step[0]) for j in range(0,len(data[0]), step[1])]
  minmax = list((lambda tp: (min(tp)[1:3],max(tp)[1:3]))([(data[i][j],i,j) for i in range(0,len(data)) for j in range(0,len(data[0]))]))
  coords += minmax

  # Loop over the data and create a `Text` for each "pixel".
  # Change the text's color depending on the data.
  texts = []
  for (i,j) in coords:
    fw = ['normal','bold'][(i,j) in minmax]
    text = im.axes.text(j, i, valfmt(data[i][j]), fontweight=fw,
           color=textcolors[data[i][j] > threshold],
           horizontalalignment="center",
           verticalalignment="center")
    texts.append(text)

  return texts

