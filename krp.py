#!/usr/bin/python3

# This script is licensed under CC-BY-NC-SA 4.0
# Original author: bitzoid

import math
import re
from data import *
from util import *
from collections import namedtuple

version = '1.4'
class Config:
  def assembly_dir():
    return 'assemblies'
  def assembly_ext():
    return 'krpa'
  def assembly_file(trunk):
    return Config.assembly_dir()+'/'+trunk+'.'+Config.assembly_ext()

try:
  from termcolor import colored
except:
  print()
  print('WARNING: Package termcolor not installed. The output will be less intelligible.')
  print()
  def colored(text, color=None, on_color=None, attrs=None):
    return text

def head(it):
  for x in it:
    return x


def many(obj,qty = 1):
  if qty == 1:
    return obj
  if type(obj) == int or type(obj) == float or type(obj) == str or type(obj) == list:
    return obj*qty
  elif type(obj) == tuple:
    return tuple(map(lambda x: many(x,qty),obj))
  else:
    return type(obj)._make((map(lambda x: many(x,qty),obj)))

def op_plus(a,b):
  return a+b

def op_times(a,b):
  return a*b

def combine(objA,objB,op=op_plus):
  if type(objA) == int or type(objA) == float or type(objA) == str or type(objA) == list:
    return op(objA,objB)
  else: #if type(objA) == tuple and type(objB) == tuple:
    a = list(objA)
    b = list(objB)
    assert(len(a) == len(b))
    r = []
    for i in range(0,len(a)):
      r += [combine(a[i],b[i],op)]
    if type(objA) == tuple:
      return tuple(r)
    else:
      return type(objA)._make(r)

def mul(x,y,op=lambda a,b: a*b):
  if type(x) in [int,str]:
    x = float(x)
  if type(y) == [int,str]:
    y = float(y)
  assert(type(y) == float)
  if type(x) == float:
    return op(x,y)
  # assume is iterable ('pythonic style')
  result = map(lambda z: mul(z,y,op), x)
  if type(x) in [tuple,list,set]:
    return type(x)(result)
  if type(x) in [dict]:
    raise ValueError('mul/div not supported for dict types as of now')
  else: # assume nameduple
    return type(x)._make(result)

def div(x,y):
  def _div(x,y):
    if -epsilon < x and x < epsilon:
      return 0.
    elif -epsilon < y and y < epsilon:
      return float('nan')
    else:
      return x/y
  return mul(x,y,_div)

def burntime_lf(r):
  bt = 0
  if r.engine.fps.lf > epsilon:
    bt = div(r.tank.fuel.lf, r.engine.fps.lf) # U / U/s = s
    if r.engine.fps.ox > epsilon:
      bt = min(bt,div(r.tank.fuel.ox, r.engine.fps.ox))
  return bt

def burntime_sf(r):
  bt = 0
  if r.engine.fps.sf > epsilon:
    bt = div(r.tank.fuel.sf, r.engine.fps.sf)
  return bt

def burntime_xe(r):
  bt = 0
  if r.engine.fps.xe > epsilon:
    bt = div(r.tank.fuel.xe, r.engine.fps.xe)
  return bt

def accel_full(r):
  return div(Location(*r.engine.thrust), r.mass) # kN / t = m/s/s

def accel_empty(r):
  bt = burntime_lf(r)
  use = [0]
  if r.tank.fuel.lf > epsilon:
    use.append(div(bt*r.engine.fps.lf,r.tank.fuel.lf))
  if r.tank.fuel.ox > epsilon:
    use.append(div(bt*r.engine.fps.ox,r.tank.fuel.ox))
  use = max(use) # what portion of lf is burned [0..1]
  emass = r.mass - use*r.tank.fuel.lf*masslf - use*r.tank.fuel.ox*massox - r.tank.fuel.sf*masssf - r.tank.fuel.xe*massxe
  if emass < 0:
    emass = 0
  return div(Location(*r.engine.thrust), emass) # kN / t = m/s/s

# this is a conservative estimation as it does not account for the vessel getting lighter by burning fuel
# this way we have a margin of error
# note: Tsiolkovsky is MUCH more precise and should be used instead
# note: This function is _deprecated_
def dv(r):
  bt = min(burntime_lf(r),burntime_sf(r),burntime_xe(r))
  return div(Location(bt*r.engine.thrust.asl, bt*r.engine.thrust.vac), r.mass) # kN / t = m/s/s

def isp(fps_thrust, maybe_thrust=None):
  if maybe_thrust == None:
    (fps, thrust) = fps_thrust
  else:
    (fps, thrust) = (fps_thrust, maybe_thrust)
  return div(Location(*thrust), (fps.lf*masslf + fps.ox*massox + fps.sf*masssf + fps.xe*massxe) * g0)

# this is the Tsiolkovsky formula
def dv_tsi(r):
  fuel = r.tank.fuel
  if r.tank.fuel.lf > epsilon:
    bt = div(r.tank.fuel.lf, r.engine.fps.lf) # U / U/s = s
    if r.engine.fps.ox > epsilon:
      bt = min(bt,div(r.tank.fuel.ox, r.engine.fps.ox))
    fuel = Fuel(bt*r.engine.fps.lf, bt*r.engine.fps.ox, r.tank.fuel.sf, r.tank.fuel.xe, r.tank.fuel.ec)
  mass_dry = r.mass - fuel_mass(fuel)
  if mass_dry > epsilon:
    quot = math.log(div(r.mass,mass_dry))
    return many(isp((r.engine.fps, r.engine.thrust)),g0*quot)
  else:
    return Location(float('nan'),float('nan'))

def _distance_helper(r,f = lambda bt: lambda _a: None):
  a = combine(accel_full(r),(-g0,0.))
  ts = list(filter(lambda t: t >= 1., [burntime_sf(r),burntime_lf(r)]))
  if len(ts) == 0:
    ts += list(filter(lambda t: t >= 1., [burntime_xe(r)]))
  if len(ts) == 0:
    ts = [0.]
  bt = min(ts)
  return Location(*tuple(map(f(bt), a)))


def distance(r,v0=0.,s0=0.):
  # v(t) = t*a + v0
  # s(t) = 0.5*t*t*a + t*v0 + s0
  return _distance_helper(r,lambda bt: lambda _a: max(s0,bt*v0+s0,0.5 * bt * bt * _a + bt * v0 + s0))

def v_gain_g0(r):
  # v(t) = t*a + v0
  # s(t) = 0.5*t*t*a + t*v0 + s0
  return _distance_helper(r,lambda bt: lambda _a: max(0.,bt * _a))




def add_component_mass(r):
  assert(r.tank.mass >= fuel_mass(r.tank.fuel))
  return Rocket(r.mass+r.engine.mass+r.tank.mass, r.tank, r.engine)

# changes the fuel type and its assciated mass
# adjust_fuel(Fuel(1,0,1,1,1),r) drains all the oxidizer
def adjust_fuel(part,r):
  fuel = combine(part,r.tank.fuel,op=op_times)
  oldfmass = fuel_mass(r.tank.fuel)
  newfmass = fuel_mass(fuel)
  tank = Tank(r.tank.mass + newfmass - oldfmass, fuel, list(map(lambda n: n + ' (adjusted)',r.tank.name)))
  r2 = Rocket(r.mass + newfmass - oldfmass, tank, r.engine)
  return r2

def drain_fuel(r):
  return adjust_fuel(Fuel(0,0,0,0,1),r)

def drain_ox(r):
  return adjust_fuel(Fuel(1,0,1,1,1),r)

_warnings_enabled = True
_warnings_stack = 0
def disable_warnings():
  global _warnings_enabled
  global _warnings_stack
  if _warnings_enabled == False:
    _warnings_stack += 1
  _warnings_enabled = False
def enable_warnings():
  global _warnings_enabled
  global _warnings_stack
  if _warnings_stack > 0:
    _warnings_stack -= 1
  else:
    _warnings_enabled = True
  
def warning(s,and_print=True):
  global _warnings_enabled
  if not _warnings_enabled:
    return ''
  r = colored('Warning: '+s,'red')
  if and_print:
    print(r)
  return r

def error(s,and_print=True):
  r = colored('Error: '+s,'red',attrs=['bold'])
  if and_print:
    print(r)
  exit(1)



def parse(expr):
  """  Grammar:
    EXPR ::= MAIN
    MAIN ::= STAGE ( ',' STAGE )*
    MASS ::= FLT ( 'g' | 'kg' | 't' | 'kt' | 'Mt' )
    STAGE ::= ROCKET ( '+' ROCKET )* ( ':' MASS )?
    ROCKET ::= ( OBJECT | COPY )
    OBJECT ::= ( FLT '*' )? ( ENGINE | PHANTOM | TANK | FUEL | MASS | SUBEXPR | ASSEMBLY | GROUP )
    SUBEXPR ::= '[' MAIN ']'
    GROUP ::= '{' MAIN '}'
    ASSEMBLY ::= '<' FILE '>'
    PHANTOM ::= '(' ENGINE ')'
    COPY ::= '!'
    ENGINE ::= NAME
    TANK ::= NAME
    FUEL ::= FLT ( 'lf' | 'ox' | 'sf' | 'xe' | 'ec' )
    NAME ::= [a-z_]+[0-9]*[a-z_]*[0-9]*[a-z_]*
    FILE ::= [^|<>/+,;:=[]]+
    FPT ::= [0-9]+ ( '.' [0-9]*)?
  Note: PHANTOM takes the specified engine's thrust and fps, but ignories its weight. Useful when upper stage engines are active.
        COPY-phantome's all engines from the next stage in this one
  Note: SUBEXPR will be reduced to the total mass of the expr.
  Note: An ASSEMBLY can evaluate to MAIN, not to EXPR. It behaves like a GROUP.
  Note: If an GROUP evaluates to more than one STAGE, it will introduce new stages."""
  fpt = '[0-9]+(\.[0-9]*)?'
  lt = '[a-z_]+[0-9]*[a-z_]*[0-9]*[a-z_]*'
  nm = '('+lt+'|\('+lt+'\)|!)'
  multi = fpt+'\*'
  pattern_mass = re.compile('^('+fpt+')(g|kg|t|kt|Mt)$')
  pattern_elem = re.compile('^'+nm+'$')
  pattern_fuel = re.compile('^'+fpt+'(lf|ox|sf|xe|ec)$')
  pattern_file = re.compile('^[^|<>/+,;:=\[\]]*$')
  pattern_group_anchor = re.compile('^\\\\[0-9]+$')
  def _parse_mass(s):
    m = pattern_mass.match(s)
    if m:
      m = m.groups()
      return float(m[0]) * {'g':0.000001,'kg':0.001,'t':1.,'kt':1000.,'Mt':1000000.}[m[2]]
    else:
      return False
  tm = False
  total_mass = 0.
  main = expr
  stages = Stages([])

  def _unnest(expr,op,cl,desc,max_depth=None):
    subsplit = list(map(lambda s: s.strip(),expr.split(op,1)))
    if len(subsplit) == 1:
      return None
    head = subsplit[0]
    sub = ''
    tail = None
    depth = 1
    for i in range(0,len(subsplit[1])):
      if max_depth != None and depth > max_depth:
        raise ValueError('Too deeply nested '+desc)
      if subsplit[1][i] == cl:
        depth -= 1
      if subsplit[1][i] == op:
        depth += 1
      if depth == 0: # found the correct closing brace
        tail = subsplit[1][(i+1):]
        break
      sub += subsplit[1][i]
    if depth != 0:
      raise ValueError('Unterminated '+desc+': "'+sub+'"')
    return (head,sub,tail)
  # deref ASSEMBLY
  while True:
    subsplit = _unnest(main,'<','>','file',1)
    if subsplit == None:
      break
    head,body,tail = subsplit
    if not pattern_file.match(body):
      raise ValueError('Invalid file: "'+body+'"')
    try:
      f = open(Config.assembly_file(body),'r')
      contents = f.read()
      f.close()
    except:
      raise ValueError('Cannot open file "'+body+'"')
    main = head + ' { ' + contents.strip() + ' } ' + tail
  # eof ASSEMBLY parsing
  # shrink SUBEXPR the mass of its last stage
  while True:
    subsplit = _unnest(main,'[',']','sub-expression')
    if subsplit == None:
      break
    head,body,tail = subsplit
    disable_warnings()
    main = head + str(parse(body)[-1].mass) + 't' + tail
    enable_warnings()
  # eof SUBEXPR parsing
  # parse GROUPs ...
  groups = []
  while True:
    subsplit = _unnest(main,'{','}','group')
    if subsplit == None:
      break
    head,body,tail = subsplit
    i = len(groups)
    disable_warnings()
    groups.append(parse(body))
    enable_warnings()
    main = head + '\\' + str(i) + ' ' + tail
  # eof GROUP

  mass = []
  use_engines = []
  use_tanks = []
  qualifiers = []
  for stage in map(lambda s: s.strip(),main.split(',')):
    mass.append(0.)
    use_engines.append([])
    use_tanks.append([])
    qualifiers.append(None)
    stuff = []
    (unqualified,qualifier) = (list(map(lambda s: s.strip(),stage.split(':',1)))+[None])[0:2]
    for obj in map(lambda s: s.strip(),unqualified.split('+')):
      if len(obj) == 0:
        continue
      parts = obj.split('*',1)
      multiplier = 1
      if len(parts) > 1:
        if len(parts[0]) == 0:
          raise ValueError('Passed muliplier operator but no multiplication argument')
        multiplier = float(parts[0])
      single = parts[-1]
      if _parse_mass(single) != False:
        stuff.append((multiplier,float(_parse_mass(single))))
      elif pattern_elem.match(single):
        stuff.append((multiplier,single))
      elif pattern_fuel.match(single):
        unit = single[-2:]
        qty = float(single[0:-2])
        index = ['lf','ox','sf','xe','ec']
        vector = [0.]*(len(index))
        for i in range(0,len(index)):
          if index[i] == unit:
            vector[i] = multiplier*qty
        f = Fuel._make(vector)
        t = Tank(fuel_mass(f),f,['Nameless ' + unit + ' Tank'])
        stuff.append((1.,t))
      elif pattern_group_anchor.match(single):
        stuff.append((multiplier,groups[int(single[1:])]))
      else:
        raise ValueError('parse error at "'+obj+'"')
    for item in stuff:
      known = 0
      if type(item[1]) == float:
        mass[-1] += item[0]*item[1]
      elif type(item[1]) == str:
        if item[1] in engine:
          use_engines[-1].append(many(engine[item[1]],int(item[0])))
          known = 1
        elif len(item[1]) > 2 and item[1][0] == '(' and item[1][-1] == ')':
          phantom = item[1][1:-1]
          if phantom in engine:
            original = engine[phantom]
            massless = Engine(0.,original.fps,original.thrust,list(map(lambda n: n+' (massless)',original.name)))
            use_engines[-1].append(many(massless,int(item[0])))
            known = 1
          else:
            raise ValueError('unknown phantom engine: "'+phantom+'"')
        elif item[1] == '!': #phantom copy all
          if len(use_engines) < 2:
            raise ValueError('cannot phantom copy all engines from previous stage in upper-most stage.')
          for e in use_engines[-2]:
            massless = Engine(0.,e.fps,e.thrust,list(map(lambda n: n+' (massless)',e.name)))
            use_engines[-1].append(many(massless,int(item[0])))
          known = 1
        if item[1] in tank:
          use_tanks[-1].append(many(tank[item[1]],int(item[0])))
          known = 1
        if known == 0:
          raise ValueError('parse error at unknown object: "'+str(item)+'"')
      elif type(item[1]) == Tank:
        use_tanks[-1].append(many(item[1],int(item[0])))
      elif type(item[1]) == Engine:
        use_engines[-1].append(many(item[1],int(item[0])))
      elif type(item[1]) == list: # it's a group
        gstages = list(map(lambda r: many(r,int(item[0])),item[1]))
        if len(gstages):
          accmass = 0.
          for gst in range(0,len(gstages)):
            mass[-1] += gstages[gst].mass - gstages[gst].engine.mass - gstages[gst].tank.mass - accmass
            use_tanks[-1].append(gstages[gst].tank)
            use_engines[-1].append(gstages[gst].engine)
            accmass += gstages[gst].mass
            if gst+1 != len(gstages):
              mass.append(0.)
              use_tanks.append([])
              use_engines.append([])
              qualifiers.append(None)
    if qualifier != None:
      q = _parse_mass(qualifier)
      if q == False:
        raise ValueError('Invalid qualifier: "'+str(qualifier)+'"')
      qualifiers[-1] = q

  assert(len(mass) == len(use_engines))
  assert(len(mass) == len(use_tanks))
  assert(len(mass) == len(qualifiers))

  for st in range(0,len(mass)):
    if len(use_tanks[st]) == 0:
      warning('Stage '+str(len(stages))+' has no tank.')
      use_tanks[st].append(tank['nullt'])
      #raise ValueError('no tanks specified')
    if len(use_engines[st]) == 0:
      warning('Stage '+str(len(stages))+' has no engine.')
      use_engines[st].append(engine['nulle'])
      #raise ValueError('no engines specified')
    e = use_engines[st][0]
    t = use_tanks[st][0]
    for i in range(1,len(use_engines[st])):
      e = combine(e,use_engines[st][i])
    for i in range(1,len(use_tanks[st])):
      t = combine(t,use_tanks[st][i])
    pl_mass = mass[st] + t.mass + e.mass
    if len(stages) and not tm:
      pl_mass += stages[-1].mass
    if qualifiers[st] == None:
      stages.append(Rocket(pl_mass,t,e))
    else:
      if qualifiers[st] + epsilon < pl_mass:
        warning('Rocket parts have more mass ('+pretty_float(pl_mass)+'t) than specified: '+pretty_float(qualifiers[st])+'t')
      stages.append(Rocket(qualifiers[st],t,e))

  if len(stages) == 0:
    raise ValueError('No stages specified.')
  if tm:
    um = total_mass
    for j in range(len(stages),0,-1):
      i = j-1
      new_um = um - stages[i].mass
      #print('This stage mass: ' + str(um) + '. Payload: ' + str(new_um) + '')
      stages[i] = Rocket(um,stages[i].tank,stages[i].engine)
      um = new_um
    if um < 0.:
      warning('Rocket parts have more mass than specified! Difference: '+str(-um))
  return stages

def parse_craft(craftfile):
  Sec = namedtuple('Sec','name,attrs,subs')
  data = ('ROOT\n { \n'+craftfile+'\n}\n')
  rxp = re.compile('[A-Z]+[a-zA-Z]*[ \t\r\n]*{[^{}]*}')
  pts_pattern = re.compile('^[^{}A-Z]*([A-Z]+[A-Za-z]*)[^A-Z{}]*{([^{}]*)}')
  empty_pattern = re.compile('^.*[^ \t\r\n].*$')
  parts = []
  while True:
    mo = rxp.search(data)
    if mo:
      sp = mo.span()
      chunks = [data[0:sp[0]], data[sp[0]:sp[1]], data[sp[1]:]]
      i = len(parts)
      parts.append(chunks[1])
      data = chunks[0] + '@' + str(i) + chunks[2]
    else:
      break
  assert(len(parts))
  def fill_in(i):
    obj = parts[i]
    if type(obj) == str:
      pts = pts_pattern.search(obj)
      nm = pts.group(1)
      bd = list(filter(empty_pattern.match, pts.group(2).splitlines()))
      subs = []
      attrs = {}
      for ln in bd:
        sub = ln.split('@',1)
        if len(sub) > 1: # is sub
          subs.append(fill_in(int(sub[1])))
        else: # is attr
          eq = ln.split('=',1)
          key = eq[0].strip()
          value = eq[1].strip()
          if key in attrs:
            if type(attrs[key]) != list:
              attrs[key] = [attrs[key]]
            attrs[key].append(value)
          else:
            attrs[key] = value
      return Sec(nm,attrs,subs)
  tree = fill_in(len(parts)-1)
  assert(tree.name == 'ROOT')
  def find_part(ident,t=tree):
    if 'part' in t.attrs and t.attrs['part'] == ident:
      return t
    for s in t.subs:
      r = find_part(ident,s)
      if r != None:
        return r
    return None
  pto = {}
  def ctl(glob):
    if type(glob) == list:
      return glob
    return [glob]
  def build_ptree_index(subtree):
    if subtree.name == 'PART':
      pto[subtree.attrs['part']] = set()
      conn = []
      if 'attN' in subtree.attrs:
        conn += ctl(subtree.attrs['attN'])
      if 'srfN' in subtree.attrs:
        conn += ctl(subtree.attrs['srfN'])
      if 'link' in subtree.attrs:
        conn += ctl(subtree.attrs['link'])
      for c in set(conn):
        value = '_'.join(c.split(',',1)[-1].split(',',1)[0].split('|')[0].split('_')[0:2])
        pto[subtree.attrs['part']].add(value)
    for s in subtree.subs:
      build_ptree_index(s)
  build_ptree_index(tree)
  PTree = namedtuple('PTree','ident,cut,ignite,sym,children')
  pstages = {0:{}}
  def ptree(ident,trail=[]):
    children = []
    trail2 = trail + [ident]
    assert(ident in pto)
    for ch in filter(lambda x: x not in trail2, pto[ident]):
      children.append(ptree(ch,trail2))
    node = find_part(ident)
    decouple = None
    ignite = None
    sym = 1
    if 'sym' in node.attrs:
      sym = len(ctl(node.attrs['sym']))+1
    decouple_m = list(filter(lambda n: n.name == 'MODULE' and 'name' in n.attrs and (n.attrs['name'] == 'ModuleDecouple' or n.attrs['name'] == 'ModuleAnchoredDecoupler'),node.subs))
    if len(decouple_m):
      decouple_m = decouple_m[0]
      if decouple_m.attrs['isEnabled'] == 'True' and decouple_m.attrs['stagingEnabled'] == 'True':
        decouple = int(node.attrs['istg'])
        if decouple not in pstages:
          pstages[decouple] = {}
        if sym not in pstages[decouple]:
          pstages[decouple][sym] = []
    ignite_m = list(filter(lambda n: n.name == 'MODULE' and 'name' in n.attrs and (n.attrs['name'] == 'ModuleEngines' or n.attrs['name'] == 'ModuleEnginesFX'),node.subs))
    if len(ignite_m):
      ignite_m = ignite_m[0]
      if ignite_m.attrs['isEnabled'] == 'True' and ignite_m.attrs['stagingEnabled'] == 'True':
        ignite = int(node.attrs['istg'])
    return PTree(ident,decouple,ignite,sym,children)
  def dissect(ptr,stg=0):
    if ptr.cut != None:
      stg = ptr.cut
    if ptr.ignite != None and ptr.ignite > (stg+1):
      for i in range(stg+1,ptr.ignite):
        if i not in pstages:
          pstages[i] = {}
        if ptr.sym not in pstages[i]:
          pstages[i][ptr.sym] = []
        pstages[i][ptr.sym].append([ptr.ident])
    if stg not in pstages:
      pstages[stg] = {}
    if ptr.sym not in pstages[stg]:
      pstages[stg][ptr.sym] = []
    pstages[stg][ptr.sym].append(ptr.ident)
    for ch in ptr.children:
      dissect(ch,stg)
  dissect(ptree(head(pto.keys())))
  rawt = []
  rawe = []
  rawm = []
  use = {}
  mass = {}
  for st in sorted(pstages.keys()):
    use[st] = {}
    for sym in pstages[st].keys():
      for pt in pstages[st][sym]:
        part = pt
        phantom = (type(pt) == list)
        if phantom:
          part = pt[0]
        oname = part.split('_',1)[0].replace('.','_')
        if oname in official_parts['FuelTank'] and official_parts['FuelTank'][oname][0] != '?':
          rawt.append((st,sym,official_parts['FuelTank'][oname]))
          assert(not phantom)
        elif oname in official_parts['Engine'] and official_parts['Engine'][oname][0] != '?':
          phbr = [['',''],['(',')']][phantom]
          rawe.append((st,sym,phbr[0]+official_parts['Engine'][oname]+phbr[1]))
        elif oname in official_parts['Electrical'] and official_parts['Electrical'][oname][0] != '?':
          rawe.append((st,sym,official_parts['Electrical'][oname]))
          assert(not phantom)
        elif oname in official_parts_mass:
          # add fuel (atm we have to manually add monoprop as we don't consider it fuel anywhere else) XXX
          tonnes = 0.
          for s in filter(lambda s: s.name == 'RESOURCE',find_part(part).subs):
            fuels = {'MonoPropellant':massmp}
            if s.attrs['name'] in fuels:
              tonnes += float(s.attrs['amount'])*fuels[s.attrs['name']]
          rawm.append((st,float(official_parts_mass[oname])+tonnes))
          assert(not phantom)
        else:
          warning('Could not match object with identifier '+oname+'.')
  for o in rawt + rawe:
    use[o[0]][o[1]] = {}
  for o in rawt + rawe:
    use[o[0]][o[1]][o[2]] = 0
  for o in rawt + rawe:
    use[o[0]][o[1]][o[2]] += 1
  for o in rawm:
    mass[o[0]] = 0
  for o in rawm:
    mass[o[0]] += o[1]
  r = []
  def mul_if_not_one(qty,s):
    if qty == 0:
      return '0t'
    elif qty == 1:
      return s
    else:
      return str(qty) + '*' + s
  for st in sorted(use.keys()):
    r.append([])
    if st in mass:
      r[-1].append(pretty_float(mass[st])+'t')
    for sym in use[st].keys():
      sub = []
      for obj in use[st][sym].keys():
        qty = use[st][sym][obj]
        assert(int(qty/sym)*sym == qty)
        sub.append(mul_if_not_one(int(qty/sym),obj))
      if sym == 1:
        r[-1] += sub
      else:
        r[-1].append(mul_if_not_one(sym, '{' + ' + '.join(sub) + '}'))
    r[-1] = ' + '.join(r[-1])
  parsed = ' , '.join(r)
  if len(parsed) > 80:
    parsed = ' ,\n'.join(r)
  print(colored(parsed,attrs=['bold']))
  main_rocket(parsed)




def pretty_float(f,prec=1000,digits=None):
  if -epsilon < f and f < epsilon:
    return '0'
  elif f == f:
    r = str(float(int(float(f)*prec+0.5)/float(prec)))
    if r[-1] == '.':
      return r[:-1]
    parts = r.split('.')
    if type(digits) == int and len(parts) > 1:
      if len(parts[0]) >= digits:
        r = str(int(float(f)+0.5))
      else:
        r = parts[0] + '.' + parts[1][:(digits-len(parts[0]))]
    return r
  else:
    return '0'

def pretty(r,terse=True):
  chunks = []
  chunks.append(pretty_float(r.mass) + 't (empty: ' + pretty_float(drain_fuel(r).mass) + 't)')
  fchunks = []
  if r.engine.fps.lf > epsilon or r.tank.fuel.lf > epsilon:
    fchunks.append((pretty_float(r.tank.fuel.lf) + 'l LF').rjust(12)+' (' + pretty_float(r.engine.fps.lf) + 'l/s)')
  if r.engine.fps.ox > epsilon or r.tank.fuel.ox > epsilon:
    fchunks.append((pretty_float(r.tank.fuel.ox) + 'l OX').rjust(12)+' (' + pretty_float(r.engine.fps.ox) + 'l/s)')
  if r.engine.fps.sf > epsilon or r.tank.fuel.sf > epsilon:
    fchunks.append((pretty_float(r.tank.fuel.sf) + 'l SF').rjust(12)+' (' + pretty_float(r.engine.fps.sf) + 'l/s)')
  if r.engine.fps.xe > epsilon or r.tank.fuel.xe > epsilon:
    fchunks.append((pretty_float(r.tank.fuel.xe) + 'l XE').rjust(12)+' (' + pretty_float(r.engine.fps.xe) + 'l/s)')
  if r.engine.fps.ec > epsilon or r.tank.fuel.ec > epsilon:
    fchunks.append((pretty_float(r.tank.fuel.ec) + 'u EC').rjust(12)+' (' + pretty_float(r.engine.fps.ec) + 'u/s)')
  if len(fchunks) == 1:
    chunks.append(fchunks[0].strip())
  elif len(fchunks) != 0:
    chunks += fchunks
  chunks.append('Thrust (ASL/Vac): ' + pretty_float(r.engine.thrust.asl) + 'kN / ' + pretty_float(r.engine.thrust.vac) + 'kN')
  #for e in r.engine.name:
  #  chunks.append(e)
  #for t in r.tank.name:
  #  chunks.append(t)
  if terse:
    return 'Rocket { ' + ', '.join(map(lambda s: s.strip(),chunks)) + ' }'
  else:
    return 'Rocket\n    ' + '\n    '.join(chunks) + ''



# does NOT include unit
def render_isp(_isp):
  return colored(pretty_float(_isp),color='blue')

# does NOT include unit
def render_dv(_dv):
  return colored(pretty_float(_dv),color='cyan',attrs=['bold'])

# does NOT include unit
def render_a(_a,in_sko=False):
  if in_sko == None:
    in_sko = True
  th = {False : [1.,4.], True : [11,14]}
  if _a != _a: # NaN
    return colored('N/A',color='red',attrs=['bold'])
  elif _a < th[in_sko][0]:
    return colored(pretty_float(_a),color='red',attrs=['bold'])
  elif _a < th[in_sko][1]:
    return colored(pretty_float(_a),color='yellow',attrs=['bold'])
  else:
    return colored(pretty_float(_a),color='green',attrs=['bold'])

# DOES include unit
def render_distance(_s):
  color = None
  if _s == None:
    s = 'N/A'
  else:
    s = str(int(_s)) + 'm'
    if _s < 500.:
      color = 'red'
    elif _s < 1500.:
      color = 'yellow'
  return colored(s,color=color,attrs=['bold','underline'])

def render_info(arr,attrs=[]):
  s = ''
  for i in range(0,len(arr)):
    s += colored(arr[i],attrs=attrs)
  return s

def xenon_only_rocket(r):
  return r.engine.fps.lf < epsilon and r.engine.fps.ox < epsilon and r.engine.fps.sf < epsilon and r.engine.fps.xe > epsilon

def info(r, pref='',sko_stage=None,v0=0.,terse=None):
  if type(r) == Rocket:
    print('\n'.join(map(lambda s: pref+s,pretty(r,terse).split('\n'))))
    lb = ["ASL","Vac"]
    for i in [0,1]:
      attrs = []
      if xenon_only_rocket(r):
        sko_stage = False
        if i == 0:
          attrs.append('dark')
      elif (sko_stage != None) and ((i == 1) == sko_stage):
        attrs.append('dark')
      print(render_info([pref+lb[i] + ":  isp / s     = ", render_isp(isp((r.engine.fps,r.engine.thrust))[i])],attrs=attrs))
      print(render_info([pref+lb[i] + ":  dv  / m/s   = ", render_dv(dv_tsi(r)[i])],attrs=attrs))
      print(render_info([pref+lb[i] + ":  a   / m/s/s = ", render_a(accel_full(r)[i],sko_stage), " -> ", render_a(accel_empty(r)[i],sko_stage)],attrs=attrs))
    if int(burntime_lf(r)):
      print(pref+"Burntime (lf): " + pretty_float(burntime_lf(r)) + 's')
    if int(burntime_sf(r)):
      print(pref+"Burntime (sf): " + pretty_float(burntime_sf(r)) + 's')
    if burntime_xe(r):
      print(pref+"Burntime (xe): " + pretty_float(burntime_xe(r)) + 's')
    if sko_stage != False:
      print(pref+"Approx. vertical distance: " + render_distance(distance(r,v0).asl))
    if r.engine.fps.ec > 0:
      print(warning(pref+"EC requirements not met",False))
  elif type(r) == Stages:
    if len(r) == 1:
      info(r[0],terse=False)
    else:
      v0 = [(0.,0.)]*(len(r)+1)
      alt = [(0.,0.)]*(len(r)+1)
      for i in range(len(r)-1,-1,-1):
        v0[i] = (v0[i+1][1], v_gain_g0(r[i]).asl + v0[i+1][1])
        alt[i] = (alt[i+1][1], distance(r[i],v0[i+1][1],alt[i+1][1]).asl)
      for i in range(0,len(r)):
        print("~~ Stage " + str(i) + " ~~")
        if alt[i][1] <= kerbin_atmo_ease[0]:
          sko_stage = True
        elif alt[i][0] > kerbin_atmo_ease[1]:
          sko_stage = False
        else:
          sko_stage = None # both inside the atmo and also sort of not
        info(r[i],'['+str(i)+']\t', sko_stage, v0=v0[i][0], terse=True)
    print('')

def _(s):
  return info(parse(s))

def build(dv, min_a, payload, t, e, vac = 1):
  if type(t) == str:
    t = tank[t]
  elif type(e) == Rocket:
    e = e.tank
  if type(e) == str:
    e = engine[e]
  elif type(e) == Rocket:
    e = e.engine
  r = add_component_mass(Rocket(payload, t, e))
  numt = 1
  nume = 1
  while True:
    if numt > 10000 or nume > 1000:
      raise ValueError('Cannot build ship to spec with these parts.')
    if dv_tsi(r)[vac] < dv:
      r = combine(r,Rocket(t.mass,t,engine["nulle"]))
      numt += 1
      continue
    if accel_full(r)[vac] < min_a:
      r = combine(r,Rocket(e.mass,tank["nullt"],e))
      nume += 1
      continue
    break
  info(r)
  return (numt,nume)


def build_to_lko(payload, t, e, min_a = 11.):
  # we need to reach a dv of v0 = 3400 m/s while having at least a = 10 m/s/s
  return build(3400., min_a, payload, t, e, 0)

#decorator-generator
def object_auto_cast(of):
  def _oac(idx=0):
    def decorator(f):
      def wrapper(*args):
        args = list(args)
        if len(args):
          if type(args[0]) == str:
            args[0] = of[args[0]]
        return f(*args)
      return wrapper
    if type(idx) == int:
      return decorator
    else:
      return decorator(idx)
  return _oac

body_auto_cast = object_auto_cast(body)

@body_auto_cast
def escape(p,r=None):
  if r == None:
    r = p.radius
  return math.sqrt(2*G*p.mass/r)

@body_auto_cast
def orbit_speed(p,r):
  return math.sqrt(G*p.mass/float(p.radius+r))

def apsis(which):
  @body_auto_cast
  def _apsis(p):
    if p.orbit == None:
      return None
    return p.orbit.smi*(1+which*p.orbit.ecc)
  return _apsis

@body_auto_cast
def g(p,r = None):
  if r == None:
    r = p.radius
  return G* p.mass / r / r

def motion(a,y0,y,v0=0,stop=False,gran=100):
  if type(a) == str or type(a) == Body:
    a = lambda y: g(a,y)
  elif type(a) == float or type(a) == int:
    a = lambda y: a
  frac = y/y0
  assert(y >= 0)
  assert(y0 >= 0)
  assert(y0 >= y)
  dy = (y0-y)/float(gran)
  y_ = y0
  t,v = 0.,v0
  error = y0-y
  for i in range(0,gran):
    # let's integrate ...
    a_ = a(y_)
    dt = (math.sqrt(2*abs(a_)*dy + v*v) - v)/abs(a_)
    t += dt
    v += a_*dt
    error -= v*dt
    y_ -= dy
    if stop and v < 0:
      break
  return (t,v,error)

@body_auto_cast
def suicide_burn(p,d,v,thrust_or_rocket,mass=None,th=2):
  HS = namedtuple('SuicideBurn','absv,v,error,h,t,ffv,fft')
  if d < epsilon:
    return HS(0,0,d,0,0,0,0)
  if type(thrust_or_rocket) == list:
    assert(len(thrust_or_rocket) > 0)
    thrust_or_rocket = thrust_or_rocket[0]
  assert(int(mass == None) <= int(type(thrust_or_rocket) == Rocket))
  if mass == None:
    mass = thrust_or_rocket.mass
    thrust = thrust_or_rocket.engine.thrust
  else:
    thrust = thrust_or_rocket
  if type(thrust) == Thrust:
    thrust = thrust.vac
  def g_(height):
    return g(p,p.radius+height)
  if thrust/mass < g_(0):
    return None
  h1 = p.radius
  # let h1 converge to a close value
  while True:
    h1,hold = (v*v/2 + g_(h1)*d)/(thrust/mass),h1
    dh = abs(h1-hold)
    #print('first round:',h1,dh)
    if dh < h1/10000.:
      break
  # h1 is now a workable approximation for the real height, we can use it to determine g(h)
  # let's try and find a better one: h2
  #        v*v + 2*g(d)*d = 2*a*h + g(h)*h
  #                       = h* (2*a + g(h))
  #        h = ( (v*v) + 2*g(d)*d ) / (2*a + g(h))
  a = thrust/mass - g_(0) # let's be conservative: a positive margin of error is better than a negative
  h2 = h1
  while True:
    h2,hold = (v*v + 2*g_(d)*d) / (2*a + g_(h2)),h2
    dh = abs(h2-hold)
    #print('second round:',h2,dh)
    if dh < h2/10000.:
      break
  # third round
  #gran = 32
  gran = 4096
  def rv(h):
    # first part (free fall)
    t1,v1,e1 = motion(g_,d+p.radius,h+p.radius,v,gran=gran)
    # second part (decel)
    t2,v2,e2 = motion(lambda y: g_(y)-thrust/mass,h+p.radius,0+p.radius,v1,gran=gran)
    return HS(abs(v2),v2,e2,h,t2,v1,t1)
  hs = list(filter(lambda h: 0<h and h<d, [h1,h2]))
  if len(hs) == 0:
    return None
  hs = min(map(rv,hs))
  if hs.v < 0:
    rng = [0,hs.h]
  else:
    rng = [hs.h,d] # this is bad, but shouldn't happen often ... or ever?
  while gran < 256 or hs.absv > th or abs(rng[0]-rng[1]) > rng[0]/100: # let's nail it to down to one percent
    #assert(rng[0] != rng[1])
    if rng[0]+epsilon >= rng[1]:
      return None
    #gran = min(int(rng[1]),2*gran, 10000)
    hs = rv((rng[0] + rng[1])/2)
    rng[hs.v < 0] = hs.h
  return hs



apoapsis = apsis(+1)
periapsis = apsis(-1)

#v^2 = G*M/R <=> R = G*M/v^2

@body_auto_cast
def orbit_of_speed(p,v):
  return G*p.mass/v/v-p.radius

BodyTree = namedtuple('BodyTree','name,body,children')

def solar_tree(from_root:str=None):
  links = {}
  root = ''
  nodes = {}
  for k in body.keys():
    links[k] = []
  for k in body.keys():
    if body[k].orbit == None:
      root = k
    else:
      assert(type(body[k].orbit) == Orbit)
      p = body[k].orbit.parent
      links[p].append(k)
  def build_for(b):
    if b in nodes:
      return
    children = []
    for l in links[b]:
      build_for(l)
      children.append(nodes[l])
    nodes[b] = BodyTree(b,body[b],children)
  if from_root == None or from_root not in body:
    from_root = root
  build_for(from_root)
  return nodes[from_root]

def path(from_body,to_body):
  t = solar_tree()
  def loc(obj,subt=t):
    if subt.name == obj:
      return []
    else:
      for i in range(0,len(subt.children)):
        pth = loc(obj,subt.children[i])
        if pth != None:
          return [i]+pth
      return None
  fr,to = loc(from_body), loc(to_body)
  root = []
  while len(fr) > 0 and len(to) > 0:
    if fr[0] == to[0]:
      root.append(fr[0])
      fr = fr[1:]
      to = to[1:]
    else:
      break
  def deref(pth,rel=t):
    more = ([],rel)
    if len(pth) != 0:
      more = deref(pth[1:],rel.children[pth[0]])
    return ([rel.name] + more[0], more[1])
  r = deref(root)[1]
  fchunk = deref(fr,r)[0]
  tchunk = deref(to,r)[0]
  assert(fchunk[0] == tchunk[0])
  fchunk = list(map(lambda x: (x,-1),fchunk))
  tchunk = list(map(lambda x: (x,+1),tchunk))
  return list(reversed(fchunk))[0:-1] + [(tchunk[0][0],0)] + tchunk[1:]

@body_auto_cast
def hohmann_dv(around,r1,r2):
  mu = around.mass*G
  rr = r1+r2
  dv1 = math.sqrt(mu/r1) * (math.sqrt(2*r2/rr)-1)
  dv2 = math.sqrt(mu/r2) * (1-math.sqrt(2*r1/rr))
  return (dv1,dv2)

# approximation of plane change (assuming ciruclar orbits)
@body_auto_cast
def plane_change(p, alt, di):
  return 2.*orbit_speed(p,alt) * math.sin(di*math.pi/180./2.)


def travel(start,finish):
  pth = path(start,finish)
  course = []
  alt = 0
  for i in range(0,len(pth)):
    b = body[pth[i][0]]
    if alt == 0:
      alt = 2*b.radius
    if pth[i][1] == -1: # escape to upper body
      course.append((escape(b,alt),'Escape from '+pth[i][0]))
      alt = b.orbit.smi
    elif pth[i][1] == 0: # orbit change
      if i == 0:
        height = b.radius
        if b.atmo != None:
          height = b.atmo.height
        course.append((orbit_speed(b,height),'Launch from '+pth[i][0]))
        alt += height
      if len(pth) > 1 and not ((i+1 == len(pth)) and (b.orbit == None)):
        target_alt = b.radius
        if i+1 < len(pth):
          target_alt = body[pth[i+1][0]].orbit.smi
          relative_alt = 0
          if i+2 < len(pth):
            relative_alt = body[pth[i+2][0]].orbit.smi + 2*body[pth[i+2][0]].radius
          else:
            relative_alt = 2*body[pth[i+1][0]].radius
          target_alt += relative_alt
          if i > 0:
            di = body[pth[i+1][0]].orbit.inclination-body[pth[i-1][0]].orbit.inclination
            if abs(di) > epsilon:
              course.append((abs(plane_change(b,alt,di)),'Plane change of '+pretty_float(di)+' deg around '+pth[i][0]))
        course.append((abs(sum(hohmann_dv(b,alt,target_alt))),'Orbit change from '+str(int(alt/km))+'km to '+str(int(target_alt/km))+'km around '+pth[i][0]))
        if i+1 < len(pth):
          alt = relative_alt
    elif pth[i][1] == 1: # break
      if i+1 == len(pth):
        pt1 = abs(sum(hohmann_dv(b,alt,b.radius)))
        pt2 = orbit_speed(b,0)
        course.append((pt1+pt2,'Landing on '+pth[i][0]))
      else:
        alt -= body[pth[i+1][0]].orbit.smi
        di = body[pth[i+1][0]].orbit.inclination
        if abs(di) > epsilon:
          course.append((abs(plane_change(b,alt,di)),'Plane change of '+pretty_float(di)+' deg around '+pth[i][0]))
  return course

def gradient_fill(x, y1, y2, fc=None, tc=None, ax=None, label=(None,None), gran=100, **kwargs):
  if ax is None:
    ax = plt.gca()
  if type(label) == str:
    label = (label,label)
  cs = [fc,tc]
  for i in range(0,2):
    if cs[i] == None:
      cs[i] = (1,1,1,1)
    elif type(cs[i]) == str:
      import matplotlib.colors as mcolors
      cs[i] = tuple(mcolors.colorConverter.to_rgba(cs[i]))
    cs[i] = tuple(map(float,cs[i]))
  result = []
  for i in range(0,gran):
    y = [[],[]]
    for j in range(0,len(y1)):
      y[0].append(y1[j] + (y2[j]-y1[j])*(float(i)/gran))
      y[1].append(y1[j] + (y2[j]-y1[j])*(float(i+1+1./gran)/gran))
    c = combine(mul(cs[0],i/gran),mul(cs[1],(gran-i)/gran))
    l = None
    if i == 0:
      l = label[0]
    if i == gran-1:
      l = label[1]
    obj = ax.fill_between(x,y[0],y[1],color=c,linewidth=0,edgecolor=c,label=l)
    if l != None:
      result.append((obj,l))
  return result

def intersections(xs, ys, zs):
  assert(len(xs) == len(ys) == len(zs))
  if len(xs) == 0:
    return []
  result = []
  state = ys[0] < zs[0]
  if ys[0] < zs[0]+epsilon and zs[0] < ys[0]+epsilon:
    result.append(xs[0])
  for i in range(1,len(xs)):
    x,y,z = xs[i],ys[i],zs[i]
    nstate = y < z
    if nstate != state:
      result.append(x)
    state = nstate
  return result

def all_intersections(xs, yss):
  result = []
  for i in range(0,len(yss)):
    for j in range(i+1,len(yss)):
      result += intersections(xs,yss[i],yss[j])
  return result

def plot_heatmap(xbounds,ybounds,rocket_expr,output_file,locs):
  if output_file != '':
    ext = output_file.rsplit('.',1)[-1]
    known_ext = {'png':'AGG','ps':'PS','eps':'PS','pdf':'PDF','svg':'SVG','jpg':'Cairo','jpeg':'Cairo'}
    if ext not in known_ext.keys():
      raise ValueError('Unsupported extension: '+ext)
    try:
      from matplotlib import use
    except:
      raise ValueError('Module python3-matplotlib could not be loaded.')
    use(known_ext[ext])
  try:
    from matplotlib import pyplot as plt
  except:
    raise ValueError('Module python3-matplotlib could not be loaded.')

  assert(xbounds[0] <= xbounds[1])
  assert(ybounds[0] <= ybounds[1])

  fig = plt.figure(figsize=(16,9))
  plt.title(rocket_expr.replace(';','\n'))
  pat_x = re.compile('([ +,{}])\\\\x( *\*)')
  pat_y = re.compile('([ +,{}])\\\\y( *\*)')
  xs = list(range(xbounds[0],xbounds[1]+1,xbounds[2]))
  ys = list(range(ybounds[0],ybounds[1]+1,ybounds[2]))
  rs = {}
  rocket = rocket_expr
  a = []
  where = 1
  dv = lambda r: float(sum(filter(lambda dv: dv == dv, map(lambda st: dv_tsi(st)[where],r))))
  for y in ys:
    rs[y] = {}
    for x in xs:
      xrp = pat_x.sub('\\g<1>'+str(x)+'\\2',rocket)
      yrp = pat_y.sub('\\g<1>'+str(y)+'\\2',xrp)
      rs[y][x] = parse(yrp)
  a = (list(map(lambda l: list(map(dv,l.values())),rs.values())))
  im,cbar = heatmap(plt,a,ys,xs,ax=plt.gca(),cmap='magma',aspect='auto',cbarlabel='dv in m/s')
  if len(xs) <= 20 and len(ys) <= 20:
    heatmap_annotate(plt,im,data=a,valfmt=lambda f: pretty_float(f,digits=1),step=(len(ys)-1,len(xs)-1))

  if output_file != '':
    plt.savefig(output_file,dpi=max(fig.dpi,300),quality=90) # quality applies to jpg/jpeg only
  else:
    plt.show()

def plot_rockets(bounds,rocket_expr,output_file,locs):
  if output_file != '':
    ext = output_file.rsplit('.',1)[-1]
    known_ext = {'png':'AGG','ps':'PS','eps':'PS','pdf':'PDF','svg':'SVG','jpg':'Cairo','jpeg':'Cairo'}
    if ext not in known_ext.keys():
      raise ValueError('Unsupported extension: '+ext)
    try:
      from matplotlib import use
    except:
      raise ValueError('Module python3-matplotlib could not be loaded.')
    use(known_ext[ext])
  try:
    from matplotlib import pyplot as plt
  except:
    raise ValueError('Module python3-matplotlib could not be loaded.')

  assert(bounds[0] <= bounds[1])
  pat = re.compile('([ +,{}])\\\\x( *\*)')
  xs = list(range(bounds[0],bounds[1]+1,bounds[2]))

  fig = plt.figure(figsize=(16,9))
  plt.title(rocket_expr.replace(';','\n'))
  ax = plt.gca()
  ax.set_xlabel('x')
  xticks = xs
  if len(xticks) > 32:
    last = xticks[-1]
    xticks = xticks[0::int(len(xticks)/20)]
    if xticks[-1] != last:
      xticks += [last]

  ax_ac = ax
  ax_dv = plt.twinx()
  ax_ac.tick_params(axis='y',color='black')
  ax_dv.tick_params(axis='y',color='black')
  ax_ac.set_ylabel('acceleration in m/s/s',color='black')
  ax_dv.set_ylabel('dv in m/s',color='black')
  ax_dv.axhline(0,color=(1,1,1,.1),linestyle='-',zorder=-1)
  ax_ac.axhline(0,color=(1,1,1,.1),linestyle='-',zorder=-1)

  import matplotlib.colors as mcolors
  rgba = lambda s: list(mcolors.colorConverter.to_rgba(s))

  palette = [('ASL',[rgba('grey' ),   (0.9,0.4,0.4,0.6),(0.4,0.9,0.4,0.6)]),
             ('Vac',[rgba('black'),  (1.0,0.3,0.3,0.7),(0.3,1.0,0.3,0.7)])]
  pseudo_palette = list(map(rgba,['blue','orange','purple','turquoise','yellow','pink','teal','brown','red']))

  rockets = rocket_expr.split(';')
  legends = []
  dvss = []
  for i in range(0,len(rockets)):
    lbsplits = rockets[i].rsplit('=',1)
    rocket = lbsplits[-1]
    show_empty = True
    if len(rockets) != 1:
      colorname = pseudo_palette[i]
      if len(lbsplits) == 1:
        nm = 'r'+str(i)
      else:
        nm = list(map(lambda s: s.strip(), lbsplits[0].split('/',1)))
        if len(nm) > 1:
          colorname = rgba(nm[1])
        else:
          try:
            t = rgba(nm[0])
            colorname = t
          except:
            pass
        nm = nm[0]
      pp = colorname
      ppf = pp[0:3]+[0.4]
      ppe = pp[0:3]+[0.01]
      palette = [('['+nm+'] ASL',[pp,ppf,ppe]), ('['+nm+'] Vac',[pp,ppf,ppe])]
      show_empty = False
    rs = {}
    for x in xs:
      rs[x] = parse(pat.sub('\\g<1>'+str(x)+'\\2',rocket))
    for where in locs:
      dvs = []
      afs = []
      aes = []
      for x in xs:
        r = rs[x]
        dv = float(sum(filter(lambda dv: dv == dv, map(lambda st: dv_tsi(st)[where],r))))
        af = accel_full(r[-1])[where]
        ae = accel_empty(r[-1])[where]
        dvs.append(dv)
        afs.append(af)
        aes.append(ae)
      for i in set((0,len(dvs)-1)):
        ax_dv.annotate('  '+pretty_float(rs[xs[i]][-1].mass,digits=2)+'t  ',xy=(xs[i],dvs[i]),color=many(tuple(palette[where][1][0]),0.5)[0:3],fontsize='small',fontstretch='condensed',horizontalalignment=['left','right'][i == 0],verticalalignment='center')
      dvss.append(dvs)

      if show_empty:
        l = (palette[where][0]+': a (full)',palette[where][0]+': a (empty)')
      else:
        l = (palette[where][0]+': a',None)
      legends += gradient_fill(xs,afs,aes,fc=palette[where][1][2],tc=palette[where][1][1],ax=ax_ac,label=l)
      for (_as,_i) in [(afs,1),(aes,2)]:
        mm = min(zip(_as,xs)),max(zip(_as,xs))
        if abs(mm[1][0]-mm[0][0])/(mm[0][0]+epsilon) > epsilon:
          ax_ac.axvline(mm[1][1],color=palette[where][1][_i],linestyle='--',alpha=0.4,zorder=-1)

      if where == 0:
        ax_ac.axhline(g0,color=(0.3,0.3,0.9,0.6),linestyle=':',zorder=-1)
        okay = list(filter(lambda xa: xa[1] > g0, zip(xs,afs)))
        if len(okay):
          ax_ac.axvline(okay[0][0],color=(0.3,0.3,0.9,0.6),linestyle=':',zorder=-1)

      l = palette[where][0]+': dv'
      lp, = ax_dv.plot(xs,dvs,label=l,color=palette[where][1][0],linewidth=4)
      legends.append((lp,l))
      mm = min(zip(dvs,xs)),max(zip(dvs,xs))
      if abs(mm[1][0]-mm[0][0])/(mm[0][0]+epsilon) > epsilon:
        ax_dv.axvline(mm[1][1],color=palette[where][1][0],alpha=0.5,linestyle='--',zorder=-1)

  extra_xticks = []
  for x in all_intersections(xs,dvss):
    ax_ac.axvline(x,color=(0.3,0.3,0.3),linestyle=':',alpha=0.4,zorder=-1)
    xticks.append(x)
    extra_xticks.append(x)
  xticks = sorted(xticks)

  #ax.set_xticks(xticks,labels=map(lambda x: ['','\n'][x in extra_ticks] + str(x),xticks))
  plt.xticks(xticks,labels=map(lambda x: ['','\n'][x in extra_xticks] + str(x),xticks))

  #fig.legend(loc='lower center',ncol=3)
  fig.legend(list(zip(*legends))[0],list(zip(*legends))[1],ncol=len(locs)*len(rockets),loc='lower center')

  if output_file != '':
    plt.savefig(output_file,dpi=max(fig.dpi,300),quality=90) # quality applies to jpg/jpeg only
  else:
    plt.show()



import sys
import inspect
#from functools import wraps

# complete options:
#   a list -- pick from exactly these
#   a str -- eval the str and interpret it as if it were a list
#   True -- do krp expr-lang completion
#   False -- do not autocomplete at all (this is inferred
#   None -- try to fall back to default-auto-completion
#   bool -- infer (False if no arguments, True if there are arguments)
Main = namedtuple('Main','f,args,amin,amax,doc,complete')

mains = {}


#decorator
def is_main(doc=None,complete=Main):
  def decorator(func):
    ps = inspect.signature(func).parameters.values()
    mm = [0,0]
    for p in list(ps):
      assert(type(p) == inspect.Parameter)
      mm[1] += 1
      if p.default == inspect.Parameter.empty:
        mm[0] += 1
    mains[func.__name__.split('main_',1)[-1].replace('_','-')] = Main(func,ps,mm[0],mm[1],doc,complete)
    return func
  return decorator

@is_main('Displays this help.',complete='list(map(lambda k: "--"+k, mains.keys()))')
def main_help(switch: 'Shows help on all commands starting with this prefix.' = None):
  def argfmt(arg):
    return colored(arg,attrs=['bold'])
  if switch == None:
    print('KRP: Try out how a rocket will behave before building it.')
    print()
    print('Example')
    print('  '+sys.argv[0]+' 0.5t + t400 + terrier, 0.4t + t800 + reliant, 0.4t + ! + 2*{2*t800 + reliant}')
    print()
    print('Commands')
    switch = ''
  l = 1
  if switch.startswith('--'):
    switch = switch[2:]
  for m in sorted(mains.keys()):
    l = max(l,len(m))
  for m in sorted(mains.keys()):
    if not m.startswith(switch):
      continue
    if mains[m].doc == None:
      continue
    argstr = []
    if mains[m].amax == 0:
      argstr = []
    for a in mains[m].args:
      nm = a.name
      if a.default != inspect.Parameter.empty:
        nm = '[' + nm + ']'
      argstr.append((nm,str(a.annotation)))
    print('  --'+m.ljust(l,' ')+'    '+('   '.join(map(lambda x: argfmt(x[0]),argstr))),end='')
    if len(argstr):
      print()
      print('    '+' '.ljust(l,' ')+'    ',end='')
    print(mains[m].doc.split('\n')[0])
    for d in mains[m].doc.split('\n')[1:]:
      print('    '+' '.ljust(l,' ')+'    '+d)
    if len(argstr) != 0:
      print('    '+' '.ljust(l,' ')+'    Arguments')
      for s in argstr:
        print('    '+' '.ljust(l,' ')+'        '+argfmt(s[0])+': '+s[1])
    print()

@is_main('Prints the syntax for rocket expressions.')
def main_syntax():
  print(parse.__doc__)

@is_main('Displays the version number.')
def main_version():
  print('krp ' + version)

def fltfmt(flt):
  parts = (str(float(flt))+'t').split('.')
  return str(parts[0]).rjust(2,' ') + '.' + str(parts[1]).ljust(5,' ')

@is_main('Lists all known engines and their identifiers.',list(engine.keys()))
def main_engines(engine_prefix: 'Show engines matching this prefix. Leave empty to list all engines.' = None):
  cats = {'lf': [], 'sf': [], 'xe': [], 'ec': [], 'NA': []}
  for e in sorted(engine.keys(),key=lambda e: engine[e].mass):
    if engine_prefix != None and not e.startswith(engine_prefix):
      continue
    if engine[e].fps.lf > epsilon and engine[e].thrust.vac > epsilon:
      c = 'lf'
    elif engine[e].fps.sf > epsilon and engine[e].thrust.vac > epsilon:
      c = 'sf'
    elif engine[e].fps.xe > epsilon and engine[e].thrust.vac > epsilon:
      c = 'xe'
    elif engine[e].fps.ec < -epsilon:
      c = 'ec'
    else:
      c = 'NA'
    cats[c].append(e)
  for c in cats.keys():
    print({'lf':'Liquid Fuel Engines', 'sf':'Solid Fuel Boosters', 'xe':'Xenon Engines', 'ec':'EC-Generators', 'NA':'Other'}[c])
    for e in cats[c]:
      print('  '+(e.ljust(10,' ')+' ('+fltfmt(engine[e].mass)+')').ljust(16,' ') + '  ' + engine[e].name[0])
    #print(' '.ljust(10,' ')+'Fps:    '+str(engine[e].fps))
    #print(' '.ljust(10,' ')+'Thrust: '+str(engine[e].thrust))

@is_main('Lists all known tanks and their identifiers or just a specific one.',list(tank.keys()))
def main_tanks(tank_prefix: 'Show tanks matching this prefix. Leave empty to list all tanks.' = None):
  cats = {'lf': [], '_lf': [], 'rf': [], 'sf': [], 'xe': [], 'ec': [], 'NA': []}
  for t in sorted(tank.keys(),key=lambda e: tank[e].mass):
    if tank_prefix != None and not t.startswith(tank_prefix):
      continue
    if tank[t].fuel.lf > epsilon and tank[t].fuel.ox > epsilon:
      c = 'rf'
    elif tank[t].fuel.lf > epsilon:
      if t[-3:] == '_lf':
        c = '_lf'
      else:
        c = 'lf'
    elif tank[t].fuel.sf > epsilon:
      c = 'sf'
    elif tank[t].fuel.xe > epsilon:
      c = 'xe'
    elif tank[t].fuel.ec > epsilon:
      c = 'ec'
    else:
      c = 'NA'
    cats[c].append(t)
  for c in cats.keys():
    if len(cats[c]):
      print({'lf':'Liquid Fuel Tanks', '_lf':'Oxigen Drained Tanks', 'rf':'Rocket Fuel Tanks', 'sf':'Solid Fuel Booster Tanks', 'xe':'Xenon Containers', 'ec':'Batteries', 'NA':'Other'}[c])
      for t in cats[c]:
        print('  '+(t.ljust(12,' ')+' ('+fltfmt(pretty_float(tank[t].mass))+')').ljust(16,' ') + '  ' + tank[t].name[0])

@is_main('Gives information on a given celestial body (not just planets).\nIf no planet is passed, prints info about all known bodys.',list(body.keys()))
def main_planet(planet: 'The object you want information for.' = None):
  consider = []
  if planet == None:
    consider = body.keys()
  elif planet in body:
    consider = [planet]
  else:
    error('Unknown celestial body: ' + planet)
  data = []
  for p in consider:
    data.append((p,[]))
    data[-1][1].append('g0: ' + pretty_float(g(p)) + 'm/s/s')
    data[-1][1].append('Escape dv: ' + str(int(escape(p)+0.5)) + 'm/s')
    if body[p].atmo != None:
      data[-1][1].append('Escape dv from atmo-edge: ' + str(int(escape(p,body[p].radius+body[p].atmo.height)+0.5))+'m/s')
      data[-1][1].append('Orbital speed at atmo-edge: '+ str(int(orbit_speed(p,body[p].atmo.height)+0.5))+'m/s')
    if body[p].orbit != None:
      data[-1][1].append('Ap: '+str(int(apoapsis(p)/Mm))+'Mm, Pe: '+str(int(periapsis(p)/Mm))+'Mm')
  l = max(map(lambda pa: len(pa[0]),data))
  i = 0
  for (p,ds) in data:
    print((p+':').ljust(l+2)+'\n'.ljust(l+3).join(ds))
    i += 1
    if i < len(data):
      print('')

@is_main('Parses a rocket and computes its flight properties.\nGenerally, you do not need to pass this flag as it is the default.')
def main_rocket(rocket_expr: 'An expr that encodes a (multi-staged) rocket. See --syntax for more information.'):
  try:
    has_x = rocket_expr.find('\\x') != -1
    has_y = rocket_expr.find('\\y') != -1
    if has_x and has_y:
      return main_plot('[1:16,1:16]',rocket_expr)
    elif has_x:
      return main_plot('[1:16]',rocket_expr)
    elif re.compile('^[^|<>/+,;:=\[\]]+ *= *[^=]+$').match(rocket_expr):
      eq = list(map(lambda s: s.strip(),rocket_expr.split('=')))
      assert(len(eq) == 2)
      return main_save(eq[0],eq[1])
    else:
      for rocket in rocket_expr.split(';'):
        info(parse(rocket))
  except ValueError as msg:
    error(str(msg))

@is_main('Plots a graph for various configuations of rockets.')
def main_plot(bounds: 'Bounds for variables \\x, \\y. E.g.: [1:8] or [1:8,1:12]',
              rocket_expr: 'A rocket expression where multipliers can be substituted with variables. E.g.: 1t + \\x*t100 + terrier\nNote that for a multi-stage rocket, the total dv is displayed but only the lowest stage acceleration.',
              output_file: 'A filename where to write the plot (svg or png). If empty the plot will be displayed instead. E.g.: myship.svg' = None,
              where: 'Plot only "vac", "asl" or "both". Default: vac' = None):
  if output_file == None:
    output_file = ''
  if where == None:
    where = 'vac'
  if where not in 'vac,asl,both'.split(','):
    error('Invalid location: '+where)
  where = {'vac':[1],'asl':[0],'both':[1,0]}[where]
  rocket_expr = ' ' + rocket_expr + ' '
  bounds = bounds.strip()
  if bounds[0] != '[' or bounds[-1] != ']':
    raise ValueError('Invalid range spec for variable bounds.')
  var = list(map(lambda s: s.strip(),bounds[1:-1].split(',')))
  try:
    for v in var:
      if not re.compile('^[0-9]+:[0-9]+(:[1-9][0-9]*)?$').match(v):
        raise ValueError('Invalid range spec: '+v)
    bounds = list(map(lambda v: tuple(map(int,(v+':1').split(':')[0:3])),var))
    if rocket_expr.find('\\z') != -1:
      if rocket_expr.find('\\y') != -1:
        raise ValueError('Cannot have \\z and \\y variable simultaneously.')
      assert(len(bounds))
      if len(bounds) == 1:
        bounds.append((1,4))
      if rocket_expr.find(';') != -1:
        raise ValueError('Cannot have more than one rocket when \\z is present.')
      parts = rocket_expr.split('\\z')
      r = []
      for i in range(bounds[1][0], bounds[1][1]+1):
        r.append('z = ' + str(i) + ' = ' + str(i).join(parts))
      rocket_expr = '; '.join(r)
    for b in bounds:
      if b[0] > b[1]:
        raise ValueError('Lower bound must be less-or-equal than upper bound.')
    if len(bounds) == 0:
      raise ValueError('No set of bounds provided')
    elif len(bounds) > 2:
      raise ValueError('Cannot handle more than two sets of bounds.')
    elif rocket_expr.find('\\y') != -1:
      plot_heatmap(bounds[0],bounds[1],rocket_expr,output_file,where)
    else:
      plot_rockets(bounds[0],rocket_expr,output_file,where)
  except ValueError as msg:
    error(str(msg))

@is_main('Try to construct a rocket from the specified parts.',complete=False)
def main_build(dv: 'Desired delta-v (in m/s).', min_a: 'Minimum acceleration (in m/s/s).', payload: 'Rocket payload (in t).', use_tank: 'Component to use as tank.', use_engine: 'Component to use as engine.', vac: 'Design for vaccum (vac) or for kerbin sea level (asl).'):
  try:
    dv = float(dv)
  except:
    error('Invalid value for dv: ' + dv)
  try:
    min_a = float(min_a)
  except:
    error('Invalid value for min_a: ' + min_a)
  try:
    payload = float(payload)
  except:
    error('Invalid value for payload: ' + payload)
  try:
    ut = tank[use_tank]
  except:
    error('Object ' + use_tank + ' is not a valid tank.')
  try:
    ue = engine[use_engine]
  except:
    error('Object ' + use_engine + ' is not a valid engine.')
  try:
    if vac == 'vac':
      vac = True
    elif vac == 'asl':
      vac = False
    else:
      vac = bool(vac)
  except:
    error('Invalid value for vac: ' + vac)
  def plural_s(qty):
    return ['s',''][qty == 1]
  try:
    (nt,ne) = build(dv,min_a,payload,ut,ue,int(vac))
    print(colored('Used '+str(nt)+' '+use_tank+' tank'+plural_s(nt)+' and '+str(ne)+' '+use_engine+' engine'+plural_s(ne)+'.',attrs=['bold']))
  except ValueError as msg:
    error(str(msg))

@is_main('Like --build, but assume we want to reach low kerbin orbit (YMMV).',complete=False)
def main_lko(payload: 'Rocket payload (in t).', use_tank: 'Component to use as tank.', use_engine: 'Component to use as engine.'):
  return main_build(3400.,11.,payload,use_tank,use_engine,'asl')

@is_main('Compute distance to ground for suicide burn. Does NOT take into account atmosphere.')
def main_suiburn(planet: 'On which body you wish to land. E.g: moho.',
                 distance: 'Distance to the ground (not to sea level!) with units or in m. E.g. 7000, 7000m, 7km all specify the same.',
                 vspeed: 'Initial vertical speed in m/s. Providing surface speed will give a margin of error.',
                 rocket: 'Rocket expr. See --syntax for further information. E.g: "2*poodle : 10t"'):
  if planet not in body:
    error('Unknown celestial body: '+planet)
  try:
    d = re.compile('^([0-9]+(\.[0-9]*)?)(m|km)$').match(distance).groups()
    distance = float(d[0]) * {'':1.,'m':1.,'km':1000.}[d[2]]
  except:
    error('Invalid distance: '+str(distance))
  try:
    vspeed = float(vspeed)
    assert(vspeed >= 0)
  except:
    error('Invalid vspeed: '+str(vspeed))
  try:
    disable_warnings()
    rocket = parse(rocket)[0]
    enable_warnings()
  except:
    error('Invalid rocket: '+str(rocket))
  hs = suicide_burn(planet,distance,vspeed,rocket)
  if hs == None:
    error('Cannot compute burn for that approach. Best of luck though.')
  else:
    print('FreeFall: '+(str(int(hs.fft+0.5))+'s').rjust(8))
    print('Altitude: '+colored((str(int(hs.h+0.5))+'m').rjust(8) + ' ('+str(int(hs.ffv+0.5))+'m/s)',attrs=['bold']))
    bt = []
    if rocket.engine.fps.lf > epsilon:
      bt.append(('lf',rocket.engine.fps.lf*hs.t))
    if rocket.engine.fps.sf > epsilon:
      bt.append(('sf',rocket.engine.fps.sf*hs.t))
    if rocket.engine.fps.xe > epsilon:
      bt.append(('xe',rocket.engine.fps.xe*hs.t))
    bt = ', '.join(map(lambda lbqt: pretty_float(lbqt[1])+lbqt[0],filter(lambda lbqt: lbqt[1] > epsilon,bt)))
    if len(bt):
      bt = ' ('+bt+')'
    print('Burntime: '+(str(int(hs.t))+'s').rjust(8)+ bt)

@is_main('Creates a subassambly to a file so it can be reused later')
def main_save(name: 'The identifier to use as filename.', rocket: 'The rocket expression to save.'):
  excl = '|<>/+,;:\[\]'
  pattern_file = re.compile('^[^'+excl+']*$')
  if not pattern_file.match(name):
    error('Invalid file name "'+name+'". It must not contain any of these characters: '+excl)
  try:
    f = open(Config.assembly_file(name),'w')
    f.write(rocket.strip())
    f.close()
  except:
    error('Could not open/write file "' + Config.assembly_file(name) + '".')
  r = main_rocket(rocket)
  print(colored('Wrote subassembly to <'+name+'>.',color='green',attrs=['bold']))
  return r

@is_main('List all known subassemblies.')
def main_assemblies():
  try:
    ass = []
    l = 0
    for f in filter(lambda f: f.endswith('.'+Config.assembly_ext()), os.listdir(Config.assembly_dir())):
      ass.append((colored('<'+f.rsplit('.',1)[0]+'>',attrs=['bold']),f.rsplit('.',1)[0]))
      l = max(l,len(ass[-1][0]))
  except:
    error('Could not access subassembly-directory.')
  if len(ass):
    for a in ass:
      try:
        f = open(Config.assembly_file(a[1]),'r')
        contents = f.read()
      except:
        warning('Could not open/read subassembly "' + a[1] + '".')
      finally:
        f.close()
      expr,mass = contents.strip(),''
      disable_warnings()
      r = parse(expr)
      enable_warnings()
      if len(r):
        mass = (str(r[-1].mass)+'t')
      else:
        mass = ''
      print(a[0].ljust(l)+' '+mass.rjust(10)+'       '+expr)


@is_main('Parses a .craft file (VERY! EXPERIMENTAL)',complete=None)
def main_craft(filename: 'Path to the .craft file.'):
  try:
    f = open(filename,'r')
    contents = f.read()
  except:
    error('Could not open/read file "' + filename + '".')
  finally:
    f.close()
  info(parse_craft(contents))

@is_main('Attempts to approximate the necessary dv to travel from one body to another.\nDoes NOT take into account atmo drag (neither when launching nor landing).',complete=list(body.keys()))
def main_travel(start_at: 'Where to take off from.', destination: 'Where to land.'):
  if start_at not in body:
    error('Unknown body '+start_at)
  if destination not in body:
    error('Unknown body '+destination)
  log = []
  if start_at != 'kerbol' or destination != 'kerbol':
    log = travel(start_at,destination)
  tally = 0.
  for (dv,note) in log:
    tally += int(dv)
    print(str(int(dv)).rjust(6)+'m/s: '+note)
  print('Required dv: '+str(int(tally))+'m/s')

import os
import subprocess

@is_main()
def main_compgen(line, point, path, cur, prev):
  def default_cmpl(cur, dictionary=None, enable_mul=1):
    if dictionary == None:
      dictionary = list(engine.keys()) + list(tank.keys())
    suggestions = []
    for obj in dictionary:
      csplit = cur.split('*',int(enable_mul))
      if obj.startswith(csplit[-1]):
        suggestions.append(['',csplit[0]+'*'][len(csplit)>1] + obj)
    # note: this prints the empty string if there are no suggestions. this is imperative
    print('\n'.join(suggestions))
  if cur.startswith('--') and prev == path:
    for m in mains.keys():
      if mains[m].doc != None and m.startswith(cur[2:]):
        print('--'+m)
  elif prev.startswith('--'):
    m = prev[2:]
    if m in mains.keys():
      mc = mains[m].complete
      if mc == bool:
        mc = len(mains[m].args) != 0
      elif type(mc) == str:
        mc = eval(mc)
      if mc == None:
        pass
      elif mc == False:
        pass
      elif mc == True:
        default_cmpl(cur)
      elif type(mc) == list:
        default_cmpl(cur,mc,0)
      else:
        exit(1)
    else:
      pass
  elif prev in body:
    default_cmpl(cur,body.keys(),0)
  else:
    default_cmpl(cur)

@is_main('Prints a command to run if you want bash expansion. Intended to be run in Makefile.')
def main_setup_expansion():
  #print('_krppy() {\nCOMPREPLY=( $('+sys.argv[0]+' --compgen "$@" ) );\n};\ncomplete -F _krppy '+sys.argv[0]+';\n')
  print('complete -C \''+sys.argv[0]+' --compgen "$COMP_LINE" "$COMP_POINT"\' -o default '+sys.argv[0])

if __name__ == "__main__":
  if len(sys.argv) < 2 or len(sys.argv[1]) == 0:
    if sys.stdin.isatty():
      os.system('python3 -i ' + sys.argv[0] + ' --version')
    else:
      while True:
        ln = sys.stdin.readline()
        if ln == '':
          break
        else:
          result = eval(ln)
          if result != None:
            print(result)
      pass
  elif len(sys.argv[1])>2 and sys.argv[1][0:2] == '--':
    # might be a switch
    switch = sys.argv[1][2:]
    if switch in mains:
      args = sys.argv[2:]
      if len(args) < mains[switch].amin:
        error('Switch "--'+switch+'" expects at least '+str(mains[switch].amin) + ' arguments but ' + str(len(args)) + ' given')
      elif len(args) > mains[switch].amax:
        error('Switch "--'+switch+'" expects at most '+str(mains[switch].amax) + ' arguments but ' + str(len(args)) + ' given')
      else:
        mains[switch].f(*args)
    else:
      warning('Unknown switch: "' + sys.argv[1] + '"')
      main_help()
  else:
    main_rocket(' '.join(sys.argv[1:]))
