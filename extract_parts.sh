#!/bin/bash

GD_PATH="$HOME/.steam/steam/steamapps/common/Kerbal Space Program/GameData/"
DIRS='Squad SquadExpansion/MakingHistory'

echo 'official_parts = {'
for t in Engine FuelTank Electrical; do
  echo "  '$t' : {"
  for d in $DIRS; do
    cd "$GD_PATH$d/Parts"
    find -name '*cfg' | grep "$t/" | while read cfg; do
      (
        echo -n "    '"
        (
          cat "$cfg" \
            | grep name\ = \
            | head -n1 \
            | cut -d '=' -f 2 \
            | tr -d "\n\t\r' "
          echo -n "'                                            "
        ) | head -c32
        echo -n ": '"
        cat "$cfg" \
          | grep title\ = \
          | head -n1 \
          | cut -d '=' -f 3 \
          | tr -d "\n\t\r' "
        echo "',"
      )
    done | sort
  done
  echo '  },'
done
echo '}'

echo 'official_parts_mass = {'
  for d in $DIRS; do
    cd "$GD_PATH$d/Parts"
    find -name '*cfg' | while read cfg; do
      (
        echo -n "    '"
        (
          cat "$cfg" \
            | grep name\ = \
            | head -n1 \
            | cut -d '=' -f 2 \
            | tr -d "\n\t\r' "
          echo -n "'                                            "
        ) | head -c32
        echo -n ": '"
        cat "$cfg" \
          | grep mass\ = \
          | head -n1 \
          | cut -d '=' -f 2 \
          | cut -d '/' -f 1 \
          | tr -d "\n\t\r' "
        echo "',"
      )
    done | sort
  done
echo '}'
