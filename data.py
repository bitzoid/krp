from parts import *
from collections import namedtuple

masslf = 0.005 # t
massox = 0.005 # t
masssf = 0.0075 # t
massxe = 0.0001 # t
massmp = 0.004 # t
km = 1000.
Mm = 1000.*km
g0 = 9.80665 # m/s/s
G = 6.67e-11 # m*m*m/kg/s/s
kerbin_atmo_ease = [10*km,20*km]
epsilon = 0.00001

Fuel =   namedtuple('Fuel',   'lf,ox,sf,xe,ec')
Fps  =   Fuel
Thrust = namedtuple('Thrust', 'asl,vac')
Engine = namedtuple('Engine', 'mass,fps,thrust,name')
Tank =   namedtuple('Tank',   'mass,fuel,name')
Rocket = namedtuple('Rocket', 'mass,tank,engine')
Stages = list

Location = namedtuple('Location', 'asl,vac')

Atmo   = namedtuple('Atmo','height,pressure')
Orbit  = namedtuple('Orbit','parent,smi,inclination,ecc')
Body   = namedtuple('Body', 'mass,radius,rotation,orbit,atmo')
Dwarf = Star = Moon = Planet = Body

engine = {
  "nulle"      : Engine(0,      Fps( 0.000,   0.000,0,0,0),       Thrust(   0.000,    0.), ['Null Engine']),

  "vernor"     : Engine( 0.080, Fps(0.4236,  0.5177,0,0,  0.),    Thrust(    6.46,   12.), ['VR-N1ER Veer-Governor']),

  "ant"        : Engine( 0.020, Fps( 0.058,   0.071,0,0,  0.),    Thrust(   0.508,    2.), ['LV-1 "Ant" Liquid Fuel Engine']),
  "spider"     : Engine( 0.020, Fps( 0.063,   0.077,0,0,  0.),    Thrust(   1.793,    2.), ['LV-1R "Spider" Liquid Fuel Engine']),
  "twitch"     : Engine( 0.090, Fps( 0.506,   0.619,0,0,  0.),    Thrust(  13.793,   16.), ['24-77 "Twitch" Liquid Fuel Engine']),
  "spark"      : Engine( 0.100, Fps( 0.574,   0.701,0,0,  0.),    Thrust(  16.875,   20.), ['48-7S "Spark" Liquid Fuel Engine']),

  "nerv"       : Engine( 3.000, Fps( 1.530,      0.,0,0, -5.),    Thrust(  13.875,   60.), ['LV-N "Nerv" Atomic Rocket Motor']),
  "terrier"    : Engine( 0.500, Fps( 1.596,   1.951,0,0,  0.),    Thrust(  14.783,   60.), ['LV-909 "Terrier" Liquid Fuel Engine']),
  "swivel"     : Engine( 1.500, Fps( 6.166,   7.536,0,0, -6.),    Thrust( 167.969,  215.), ['LV-T45 "Swivel" Liquid Fuel Engine']),
  "reliant"    : Engine( 1.250, Fps( 7.105,   8.684,0,0, -7.),    Thrust( 205.161,  240.), ['LV-T30 "Reliant" Liquid Fuel Engine']),

  "thud"       : Engine( 0.900, Fps( 3.611,   4.413,0,0,  0.),    Thrust( 108.197,  120.), ['Mk-55 "Thud" Liquid Fuel Engine']),
  "dart"       : Engine( 1.000, Fps( 4.859,   5.938,0,0, -5.),    Thrust( 153.529,  180.), ['T-1 Toroidal Aerospike "Dart" Liquid Fuel Engine']),

  "poodle"     : Engine( 1.750, Fps( 6.555,   8.012,0,0, -8.),    Thrust(  64.286,  250.), ['RE-L10 "Poodle" Liquid Fuel Engine']),
  "skipper"    : Engine( 3.000, Fps(18.642,  22.784,0,0,-10.),    Thrust( 568.750,  650.), ['RE-I5 "Skipper" Liquid Fuel Engine']),
  "mainsail"   : Engine( 6.000, Fps(44.407,  54.275,0,0,-12.),    Thrust(1379.032, 1500.), ['RE-M3 "Mainsail" Liquid Engine']),

  "rhino"      : Engine( 9.000, Fps(53.985,  65.982,0,0,-12.),    Thrust(1205.882, 2000.), ['Kerbodyne KR-2L+ "Rhino" Liquid Fuel Engine']),
  "vector"     : Engine( 4.000, Fps(29.135,  35.609,0,0, -3.),    Thrust( 936.508, 1000.), ['S3 KS-25 "Vector" Liquid Fuel Engine']),
  "twinboar"   : Engine(10.500, Fps(61.183,  74.779,0,0,  0.),    Thrust(1866.667, 2000.), ['LFB KR-1x2 "Twin-Boar" Liquid Fuel Engine']),  # note: this is the dry mass
  "mammoth"    : Engine(15.000, Fps(116.539,142.437,0,0,-12.),    Thrust(3746.032, 4000.), ['S3 KS-25x4 "Mammoth" Liquid Fuel Engine']),

  "sepratron"  : Engine( 0.010, Fps(0,      0., 1.590,0,  0.),    Thrust(  13.790,   18.), ['Sepratron I']),                                # note: this is the dry mass
  "flea"       : Engine( 0.450, Fps(0,      0.,15.800,0,  0.),    Thrust( 162.910,  192.), ['RT-5 "Flea" Solid Fuel Booster']),             # note: this is the dry mass
  "hammer"     : Engine( 0.750, Fps(0,      0.,15.810,0,  0.),    Thrust( 197.900,  227.), ['RT-10 "Hammer" Solid Fuel Booster']),          # note: this is the dry mass
  "thumper"    : Engine( 1.500, Fps(0,      0.,19.400,0,  0.),    Thrust( 250.000,  300.), ['BACC "Thumper" Solid Fuel Booster']),          # note: this is the dry mass
  "kickback"   : Engine( 4.500, Fps(0,      0.,41.407,0,  0.),    Thrust( 593.864,  670.), ['S1 SRB-KD25k "Kickback" Solid Fuel Booster']), # note: this is the dry mass

  "dawn"       : Engine( 0.250, Fps(0,      0.,0,0.486,8.74),     Thrust(0.048,       2.), ['IX-6315 "Dawn" Electric Propulsion System']),

  "ox4"        : Engine(0.0175, Fps(0,      0.,0,0,-1.6),         Thrust(0,0)            , ['OX-4L 1x6 Photovoltaic Panels']),
  "sp"         : Engine( 0.025, Fps(0,      0.,0,0,-1.6),         Thrust(0,0)            , ['SP-L 1x6 Photovoltaic Panels']),
  "stat"       : Engine( 0.005, Fps(0,      0.,0,0,-0.3),         Thrust(0,0)            , ['OX-STAT Photovoltaic Panels']),
  "statxl"     : Engine( 0.040, Fps(0,      0.,0,0,-2.8),         Thrust(0,0)            , ['OX-STAT-XL Photovoltaic Panels']),
  "gigantor"   : Engine( 0.300, Fps(0,      0.,0,0,-24.4),        Thrust(0,0)            , ['Gigantor XL Solar Array']),
  "fc"         : Engine( 0.050, Fps(0.0016875,0.0020625,0,0,-1.5),Thrust(0,0)            , ['Fuel Cell']),
  "fca"        : Engine( 0.240, Fps(0.02025,0.02475,0,0,-18),     Thrust(0,0)            , ['Fuel Cell Array']),
  "nuk"        : Engine( 0.080, Fps(0.,     0.,0,0,-0.8),         Thrust(0,0)            , ['PB-NUK Radioisotope Thermoelectric Generator']),

  ##### MHE #####

  "cub"        : Engine( 0.180, Fps(  0.947,  1.158,0,0, 0.),     Thrust(  28.903,   32.), ['RV-1 "Cub" Vernier engine (MHE)']),

  "c1000"      : Engine( 0.750, Fps(  0.,     0.,2.825,0,0),      Thrust(24.519,32)      , ['FL-C1000 Booster (MHE)']),                       # note: this is the dry mass

  "cheetah"    : Engine( 1.000, Fps(  3.231,  3.950,0,0,-5.),     Thrust(  52.817,  125.), ['LV-T91 "Cheetah" Liquid Fuel Engine (MHE)']),
  "kodiak"     : Engine( 1.250, Fps(  7.954,  9.721,0,0,-5.),     Thrust( 247.000,  260.), ['RK-7 "Kodiak" Liquid Fuel Engine (MHE)']),
  "skiff"      : Engine( 1.600, Fps(  8.343, 10.197,0,0,-3.),     Thrust( 240.909,  300.), ['RE-I1 "Skiff" Liquid Fuel Engine (MHE)']),
  "bobcat"     : Engine( 2.000, Fps( 11.842, 14.473,0,0,-8.),     Thrust( 374.194,  400.), ['LV-TX87 "Bobcat" Liquid Fuel Engine (MHE)']),
  "wolfhound"  : Engine( 3.300, Fps(  9.057, 11.069,0,0,-8.),     Thrust(  69.079,  375.), ['RE-J10 "Wolfhound" Liquid Fuel Engine (MHE)']),
  "mastodon"   : Engine( 5.000, Fps( 40.621, 49.648,0,0,-8.),     Thrust(1283.607, 1350.), ['Kerbodyne KE-1 "Mastodon" Liquid Fuel Engine (MHE)']),
}

tank = {
  "nullt"      : Tank( 0,      Fuel(0,0,0,0,0)          , ['Null Tank']),

  "dumpling"   : Tank(0.1238,  Fuel(   9.9,  12.1,0,0,0), ['R-4 "Dumpling" External Tank']),
  "oscarb"     : Tank(0.2250,  Fuel(  18.0,  22.0,0,0,0), ['Oscar-B Fuel Tank']),
  "baguette"   : Tank(0.3038,  Fuel(  24.3,  29.7,0,0,0), ['R-11 "Baguette" External Tank']),
  "doughnut"   : Tank(0.3375,  Fuel(  27.0,  33.0,0,0,0), ['R-12 "Doughnut" External Tank']),

  "t100"       : Tank( 0.5625, Fuel(   45.,   55.,0,0,0), ['FL-T100 Fuel Tank']),
  "t200"       : Tank( 1.125,  Fuel(   90.,  110.,0,0,0), ['FL-T200 Fuel Tank']),
  "t400"       : Tank( 2.250,  Fuel(  180.,  220.,0,0,0), ['FL-T400 Fuel Tank']),
  "t800"       : Tank( 4.500,  Fuel(  360.,  440.,0,0,0), ['FL-T800 Fuel Tank']),

  "c7"         : Tank( 4.570,  Fuel(  360.,  440.,0,0,0), ['C7 Brand Adapter - 2.5m to 1.25m']),

  "rocko8"     : Tank( 4.500,  Fuel(  360.,  440.,0,0,0), ['Rockomax X200-8 Fuel Tank']),
  "rocko16"    : Tank( 9.000,  Fuel(  720.,  880.,0,0,0), ['Rockomax X200-16 Fuel Tank']),
  "rocko32"    : Tank(18.000,  Fuel( 1440., 1760.,0,0,0), ['Rockomax X200-32 Fuel Tank']),
  "rocko64"    : Tank(36.000,  Fuel( 2880., 3520.,0,0,0), ['Rockomax Jumbo-64 Fuel Tank']),

  "kerbo36"    : Tank(20.250,  Fuel( 1620., 1980.,0,0,0), ['Kerbodyne S3-3600 Tank']),
  "kerbo72"    : Tank(40.500,  Fuel( 3240., 3960.,0,0,0), ['Kerbodyne S3-7200 Tank']),
  "kerbo144"   : Tank(81.000,  Fuel( 6480., 7920.,0,0,0), ['Kerbodyne S3-14400 Tank']),

  "mk0"        : Tank( 0.275,  Fuel(   50.,     0,0,0,0), ['Mk0 Liquid Fuel Fuselage']),
  "ncs"        : Tank( 0.500,  Fuel(   80.,     0,0,0,0), ['NCS Adapter']),
  "mk1"        : Tank( 2.250,  Fuel(  400.,     0,0,0,0), ['Mk1 Liquid Fuel Fuselage']),
  "mk2lfs"     : Tank( 2.290,  Fuel(  400.,     0,0,0,0), ['Mk2 Liquid Fuel Fuselage Short']),
  "mk2lf"      : Tank( 4.570,  Fuel(  800.,     0,0,0,0), ['Mk2 Liquid Fuel Fuselage']),
  "mk2s"       : Tank( 2.290,  Fuel(  180.,  220.,0,0,0), ['Mk2 Rocket Fuel Fuselage Short / Mk2 to 1.25m Adapter / Mk2 Bicoupler']),
  "mk2"        : Tank( 4.570,  Fuel(  360.,  440.,0,0,0), ['Mk2 Rocket Fuel Fuselage / Mk2 to 1.25m Adapter Long / 2.5m to Mk2 Adapter']),

  "mk2mk3"     : Tank(11.430,  Fuel(  900., 1100.,0,0,0), ['Mk3 to Mk2 Adapter']),

  "mk3s"       : Tank(14.290,  Fuel( 1125., 1375.,0,0,0), ['Mk3 Rocket Fuel Fuselage Short']),
  "mk3"        : Tank(28.570,  Fuel( 5000., 2750.,0,0,0), ['Mk3 Rocket Fuel Fuselage']),
  "mk3l"       : Tank(57.140,  Fuel(10000., 5500.,0,0,0), ['Mk3 Rocket Fuel Fuselage Long']),
  "mk3lfs"     : Tank(14.290,  Fuel( 2500.,     0,0,0,0), ['Mk3 Liquid Fuel Fuselage Short']),
  "mk3lf"      : Tank(28.570,  Fuel( 5000.,     0,0,0,0), ['Mk3 Liquid Fuel Fuselage']),
  "mk3lfl"     : Tank(57.140,  Fuel(10000.,     0,0,0,0), ['Mk3 Liquid Fuel Fuselage Long']),

  "mk3a"       : Tank(14.290,  Fuel( 1125., 1375.,0,0,0), ['Mk3 to 2.5m/3.75m Adapter [Slanted]']),
  "adtp"       : Tank(16.875,  Fuel( 1350., 1650.,0,0,0), ['Kerbodyne ADTP-2-3']),

  "xenon50"    : Tank( 0.054,  Fuel(   0,   0,0, 405.,0), ['PB-X50R Xenon Container']),
  "xenon150"   : Tank( 0.096,  Fuel(   0,   0,0, 720.,0), ['PB-X150 Xenon Container']),
  "xenon750"   : Tank( 0.760,  Fuel(   0,   0,0,5700.,0), ['PB-X750 Xenon Container']),

  "twinboar"   : Tank(32.000,  Fuel( 2880., 3520.,0,0,0), ['LFB KR-1x2 "Twin-Boar" Liquid Fuel']),  # note: this is ONLY the mass of the fuel, excluding dry mass
  "separatron" : Tank( 0.060,  Fuel(     0, 0,   8.,0,0), ['Sepratron I']),                         # note: this is ONLY the mass of the fuel, excluding dry mass
  "flea"       : Tank( 1.050,  Fuel(     0, 0, 140.,0,0), ['RT-5 "Flea" Solid Fuel']),              # note: this is ONLY the mass of the fuel, excluding dry mass
  "hammer"     : Tank( 2.810,  Fuel(     0, 0, 375.,0,0), ['RT-10 "Hammer" Solid Fuel']),           # note: this is ONLY the mass of the fuel, excluding dry mass
  "thumper"    : Tank( 6.150,  Fuel(     0, 0, 820.,0,0), ['BACC "Thumper" Solid Fuel']),           # note: this is ONLY the mass of the fuel, excluding dry mass
  "kickback"   : Tank(19.500,  Fuel(     0, 0,2600.,0,0), ['S1 SRB-KD25k "Kickback" Solid Fuel']),  # note: this is ONLY the mass of the fuel, excluding dry mass

  ##### MHE #####

  "a150"       : Tank( 0.900,  Fuel(   72.,   88.,0,0,0), ['FL-A150 Fuel Tank Adapter / FL-A151S Fuel Tank Adapter (MHE)']),
  "a151l"      : Tank( 3.372,  Fuel(  270.,  330.,0,0,0), ['FL-A151L Fuel Tank Adapter (MHE)']),
  "a215"       : Tank( 6.750,  Fuel(  540.,  660.,0,0,0), ['FL-A215 Fuel Tank Adapter (MHE)']),
  "c1000"      : Tank( 6.030,  Fuel(  540.,  660.,4,0,0), ['FL-C1000 Fuel (MHE)']),                 # note: this is ONLY the mass of the fuel, excluding dry mass

  "tx220"      : Tank( 1.2375, Fuel(   99.,  121.,0,0,0), ['FL-TX220 Fuel Tank (MHE)']),
  "tx440"      : Tank( 2.475 , Fuel(  198.,  242.,0,0,0), ['FL-TX440 Fuel Tank (MHE)']),
  "tx900"      : Tank( 5.0625, Fuel(  405.,  495.,0,0,0), ['FL-TX900 Fuel Tank (MHE)']),
  "tx1800"     : Tank(10.125 , Fuel(  810.,  900.,0,0,0), ['FL-TX1800 Fuel Tank (MHE)']),

  "cluster"    : Tank( 50.625, Fuel( 4050., 4950.,0,0,0), ['Kerbodyne Engine Cluster Adapter Tank (MHE)']),
  "s3s4"       : Tank( 50.625, Fuel( 4050., 4950.,0,0,0), ['Kerbodyne S3-S4 Adapter Tank (MHE)']),

  "kerbo64"    : Tank( 36.000, Fuel( 2880., 3520.,0,0,0), ['Kerbodyne S4-64 Fuel Tank (MHE)']),
  "kerbo128"   : Tank( 72.000, Fuel( 5760., 7040.,0,0,0), ['Kerbodyne S4-128 Fuel Tank (MHE)']),
  "kerbo256"   : Tank(144.000, Fuel(11520.,14080.,0,0,0), ['Kerbodyne S4-256 Fuel Tank (MHE)']),
  "kerbo512"   : Tank(288.000, Fuel(23040.,28160.,0,0,0), ['Kerbodyne S4-512 Fuel Tank (MHE)']),
}

def time(s,m,h,d=0):
  return float(((d*6 + h)*60 + m)*60 + s)

body = {
  "kerbol"   : Star  ( 1.7565459e+28,  262*Mm,   time(d=20,h= 0,m= 0,s= 0), None                                      ,  None),
  "moho"     : Planet( 2.5263314e+21,  250*km,   time(d=56,h= 0,m= 6,s=40), Orbit('kerbol',  5263138304, 7.000, 0.200),  None),
  "eve"      : Planet( 1.2243980e+23,  700*km,   time(d= 3,h= 4,m=21,s=40), Orbit('kerbol',  9832684544, 2.100, 0.010),  Atmo( 90*km,5)),
  "gilly"    : Moon  ( 1.2420363e+17,   13*km,   time(d= 5,h= 1,m=50,s=55), Orbit('eve'   ,    31500000,12.000, 0.550),  None),
  "kerbin"   : Planet( 5.2915158e+22,  600*km,   time(     h= 5,m=59,s= 9), Orbit('kerbol', 13599840256, 0.000, 0.000),  Atmo( 70*km,1)),
  "mun"      : Moon  ( 9.7599066e+20,  200*km,   time(d= 6,h= 2,m=36,s=24), Orbit('kerbin',    12000000, 0.000, 0.000),  None),
  "minmus"   : Moon  ( 2.6457580e+19,   60*km,   time(d= 1,h= 5,m=13,s=20), Orbit('kerbin',    47000000, 6.000, 0.000),  None),
  "duna"     : Planet( 4.5154270e+21,  320*km,   time(d= 3,h= 0,m=11,s=58), Orbit('kerbol', 20726155264, 0.060, 0.051),  Atmo( 50*km,1./15)),
  "ike"      : Moon  ( 2.7821615e+20,  130*km,   time(d= 3,h= 0,m=11,s=58), Orbit('duna'  ,     3200000, 0.200, 0.030),  None),
  "dres"     : Dwarf ( 2.5263314e+21,  250*km,   time(d= 1,h= 3,m=40,s= 0), Orbit('kerbol', 40839348203, 5.000, 0.145),  None),
  "jool"     : Planet( 4.2332127e+24, 6000*km,   time(d= 1,h= 4,m= 0,s= 0), Orbit('kerbol', 68773560320, 1.304, 0.050),  Atmo(200*km,15)),
  "laythe"   : Moon  ( 2.9397311e+22,  500*km,   time(d= 2,h= 2,m=43,s= 1), Orbit('jool'  ,    27184000, 0.000, 0.000),  Atmo( 50*km,0.6)),
  "vall"     : Moon  ( 3.1087655e+21,  300*km,   time(d= 4,h= 5,m=26,s= 2), Orbit('jool'  ,    43152000, 0.000, 0.000),  None),
  "tylo"     : Moon  ( 4.2332127e+22,  600*km,   time(d= 9,h= 4,m=52,s= 6), Orbit('jool'  ,    68500000, 0.025, 0.000),  None),
  "bop"      : Moon  ( 3.7261090e+19,   65*km,   time(d=25,h= 1,m=15,s= 7), Orbit('jool'  ,   128500000,15.000, 0.235),  None),
  "pol"      : Moon  ( 1.0813507e+19,   44*km,   time(d=41,h= 4,m=31,s=42), Orbit('jool'  ,   179890000, 4.250, 0.171),  None),
  "eeloo"    : Dwarf ( 1.1149224e+21,  210*km,   time(     h= 5,m=24,s=20), Orbit('kerbol', 90118820000, 6.150, 0.260),  None),
  "sol"      : Star  ( 1.9885000e+30,  696*Mm,   time(d=25,h= 9,m= 7,s=12), None, None), # inclination for solar objects refers to the 'invariable plane', atmo height corresponds to the Kármán line
  "earth"    : Star  ( 5.9723700e+24, 6378*km,   time(     h=23,m=56,s= 4), Orbit('sol'   ,149598023000, 1.57869, 0.0167086), Atmo(100*km,1)),
  "moon"     : Star  ( 7.3420000e+22, 1737*km,   time(d=27,h= 7,m=43,s=12), Orbit('earth' ,   384399000, 5.14500, 0.0549000), None),
}


def fuel_mass(fuel):
  return fuel.lf*masslf + fuel.ox*massox + fuel.sf*masssf + fuel.xe*massxe

tank_lf = dict()
for k in tank.keys():
  if tank[k].fuel.ox > epsilon:
    tank_lf[k+'_lf'] = Tank(tank[k].mass - fuel_mass(tank[k].fuel) + tank[k].fuel.lf*masslf, Fuel(tank[k].fuel.lf,0,0,0,0), list(map(lambda n: n+' (OX drained)',tank[k].name)))
for k in tank_lf.keys():
  tank[k] = tank_lf[k]
